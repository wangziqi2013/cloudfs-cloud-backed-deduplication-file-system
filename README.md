---
authors:
- 'Ziqi Wang (ziqiw)'
title: 'CloudFS: A Cloud-Backed Deduplication File System Implementation'
...

Using the power of cloud-backed object storage, capabilities of
traditional disk-based storage system are extended by both the
flexibility of cloud and high throughput of local storage. Combing them
together provides users with the convenience of utilizing cloud as a
local file system while maintaining full transparency to existing
applications. In this paper we describe a CloudFS implementation running
on Amazon S3 object storage and local SSD, with a rich feature set
including full support for traditional POSIX semantics as well as online
segment deduplication, snapshots, and roll-back.

Introduction
============

The advent of cloud technology has dramatically changed how storage
people make use of the power of computer inter-networking. The emerge of
commercial cloud service providers furthermore impelled quick and easy
adoption of cloud into traditional storage systems. Among tens of cloud
service providers, we choose Amazon S3 as the underlying cloud platform
for CloudFS, not only because the high availability and stability
promised, but also due to the simplicity of its interface specification.

Onto the cloud storage layer, we built a deduplication file system to
optimize cloud usage by content-based segmentation and squeezing common
contents from different entities into one representation, achieving
similar effects as compression. In order to apply deduplication
on-the-fly without losing generality with arbitrary read/write
sequences, immutable objects are brought to the foreground and defines
important features of our system.

Support for snapshots are crucial under the assumption that demands for
restoring data are the norm. CloudFS defines clear semantics for
snapshots and has simple implementation. Users are free to choose not to
use, though, without being punished by any performance or feature loss.

Design Overview
===============

CloudFS is characterized by a layered architecture. We describe them in
a bottom-up approach in the following sections.

Segment Naming and Migration
----------------------------

In CloudFS, segments are defined as a chunk of data that are immutable,
unique, and shared. Due to its nature of being shared, reference counts
are maintained for each segment, and segment will be destroyed once its
reference count goes down to zero.

Another important attribute of segments is identifier. Using content as
identifier makes perfect sense with deduplication, despite that we
actually message digest variable sized segments to produce shorter and
uniformly sized hash as identifiers. However, with deduplication turned
off, content-as-identifier is not feasible anymore, since segments needs
to be distinguished even if they are identical by content. To address
this problem, we design segment identifier as having two parts: A
content hash and a segment ID, separated by a period. Segment ID is a
monotonically increasing integer starting from one (zero is treated as
INVALID for return value), and is assigned to new segments once they
need to be distinguished.

The third attribute is segment location. Being able to control migration
on segment granularity introduces an overhead to keep track of locations
per segment compared with keeping this on a per file basis, but on the
other hand, segment migration enables finer grain tuning over
capacity-cost balance, and even simplifies file oblivious caching.

Complicated policy over segment migration is possible, though, only a
naive one is implemented by CloudFS: Once a file’s size exceeds
threshold, all its segments are uploaded to the cloud. Later however,
through experiments we figure out that unpractical high cloud cost
justifies researching on a better strategy.

Class $Segment$ and $segment.cpp$ implements the above functionalities.
Furthermore, it maintains a segment mapping database $SegmentMap$, which
translates MD5 content hash into in-memory segment object. Always
checking for identical segments using MD5 hash when adding new segments
implements deduplication, while turning that off and always assigning
new segment ID implements no-dedup mode.

Immutable Objects and Log Replay
--------------------------------

While Segment Placement Layer encapsulates segment migration and naming,
Extent Manager Layer organizes segments into files. An extent is always
associated with a segment, and is defined as a consecutive region in a
file that has the same content as its underlying segment. Since segments
have already taken the role as data holder and are persisted to Object
Storage Layer, an extent is simply the collection of a starting offset
in the file, and an ID field pointing to its underlying segment.

Segments and extents are both immutable objects, implying that existing
segments and extents are not affected when files are being changed.
Changes are recorded and persisted only by appending new extents to
files, and if necessary by creating new segments. The append-only rule
resembles that in log-structured file system, except that logs are
maintained on a per file basis.

Essentially, file is an array of extents, sorted by time being appended.
Writing a file is no more than appending a new extent, while reading
involves inspecting all extents, selecting those overlapping with read
range, and applying them to the read buffer.

The advantage of recording with immutable objects is tremendous. CloudFS
survives from arbitrary read/write/seek sequence, even with sparse
files, big holes (e.g. Seeking 64GB and then write one byte), and
overwriting, without confusing deduplication. However, read overhead is
problematic with files frequently overwritten, especially when segments
are fetched from the cloud.

Even under log-structured setting, sequential write is mostly favored
and optimized. Peculiar write patterns significantly degrade performance
(e.g. write one byte, seek one byte, repeat), they are not common in
normal operations though. Barely guaranteeing no more than correctness
is reasonable.

Source file $extent.cpp$ and class $Extent$ implements interval
intersection algorithm to select extents for reading, and provides an
$ApplyTo()$ method to overlay extents on a given read buffer. To
facilitate translation from segment ID to segment object, there is a
lookup table ExtentMap for use when finding an extent’s underlying
segment.

Optimistic and Pessimistic Deduplication
----------------------------------------

Deduplication performs the best if data to be processed is known in
advance, or at least fed without seek. In a file system, this implies
sequential writes taking place all the time, the generality of which is
in deep doubt (but fits into special purpose applications such as
backup).

Therefore, keeping interrupts in input stream as few as possible is
preferred, which requires predicting into the future. Given the
uncertainty of write patterns, however, optimistically assuming writes
being almost sequential would lead to potentially the worst case that
the whole file is cached in memory before decuplication became possible.
Although this is not bad at the first glance, because in-memory
structures are easily turned into on-disk objects so that disks are used
as a deduplication cache, files larger than disk space could easily blow
up the cache, while they really should not if treated more carefully
(Think of Writing one byte at offset 0, 4GB at offset 2, finally one
byte at offset 1).

Another extreme is the pessimistic approach, which does not take
advantage of correlations between writes, and deduplicate whatever there
is in one write request. The worst case is no longer bounded by file
length, though, it is now bounded by minimum write request, which is
illustrated by the following example: Write one byte, repeat until 4GB
written. Obviously, in this case metadata explodes at a rate even faster
than how actual data grows.

CloudFS is somewhere between these two. For every input stream, there is
a buffer maintained by Context Manager Layer, the size of which is set
to maximum segment size. On every write, request payload is appended to
the buffer, over which Rabin fingerprinting is run thereafter. After all
segment boundaries have been marked by Rabin, individual segments are
passed to Extent Manager for registration, and then to Segment Placement
Manager for persistence. The only exception is for the last segment in
the buffer, since we could not tell whether its tail boundary is
artificially enforced due to interruption in the stream, or a natural
boundary by Rabin. Therefore, we shift the last segment to buffer head,
and optimistically assume next write starting at where current stream is
interrupted.

If we are lucky, in the best case where writes are all sequential, this
approach behaves as if all writes going to disk first and deduplicated
as a whole, without actually consuming disk space. It is also optimal,
since no artificial boundary is enforced.

Although writing sequentially is behavior of the majority, in case next
write starting at an unexpected offset, CloudFS falls back immediately
to pessimistic, and “seals” whatever last write leaves behind. Enforcing
an artificial boundary in the buffer is called “Sealing” the buffer, and
happens when sequential write is over, or switching to read/truncate.
The latter guarantees POSIX semantics for read (read operation should
reflect most recent writes).

Source file $splitter.cpp$ and class $OnlineSegmentation$ implements
Rabin and buffering part of deduplication, while read/write sequence
control is in class $OpenedFile$, file $opened\_ file.cpp$. They
cooperate through interface $AppendData()$ and $Seal()$.

Streaming Control
-----------------

CloudFS maintains an opened file table for streaming control, in
addition to low-level states the operating system and FUSE maintains. As
suggested by the previous section, CloudFS keeps a buffer for each input
stream, together with variables recording the region affected by last
write operation. These pieces of information aggregate as an
$OpenedFile$ object in $ActiveFileMap$ indexed by FUSE path. Please
refer to $opened\_ file.cpp$ for details.

It is worth to mention that, although the operating system trivially
supports opening a file for multiple times, it is not trivial in
CloudFS. Moreover, many existing applications rely on the fact that
nested $open$-$close$ on a file only causes real file close on the last
$close$ call, it is necessary to keep a reference count to pair nested
$open$ and $close$ in $OpenedFile$ structure to avoid application
crashing.

Metadata Abstraction
--------------------

Metadata dictates how applications interact with files. Though easily
generalized to other cases, we cover only attribute operations, create,
open, close, read and write here. Truncate and unlink are discussed
later under the context of garbage collection.

Like any mainstream file system, CloudFS provides users with a name
space logically resembling a tree, with intermediate nodes being
directories and leaf nodes being files. For the sake of simplicity, file
and directory creation on ClousFS are relayed to the host file system
(usually ext4), taking maximum advantage of existing highly-optimized
code to manage the hierarchy. However, using the host file system as an
object storage, CloudFS keeps metadata in its own format, and takes the
responsibility of interfacing between VFS calls and CloudFS metadata
APIs.

To be more precise: File creation on CloudFS is interpreted as creating
an empty proxy file on the location where logically the request should
be fulfilled, the content of which is then initialized to be a set of
attributes with default value, plus an empty extent vector. Metadata
operations on CloudFS files are redirected to those stored in the proxy
file, while attributes of the proxy itself remain unchanged and private
to CloudFS.

Opening a file in CloudFS equals reading the corresponding proxy file
into memory, which is then wrapped by $OpenedFile$ structure and
registered to $ActiveFileTable$. Closing a file is interpreted as
flushing the memory object back to proxy file, persisting any changes
during the session. Caching proxy file in memory poses cache coherence
problem, without proper handling of which metadata operations being
rolled back might happen. For example, after file opening, a $chmod$
call should be redirected to the memory copy instead of blindly updating
on-disk representation regardless of file status.

File open also loads extent vector as an object, on which read and write
calls operate. The notion of immutable object and log replay have been
elaborated in previous sections, still, delicate issues regarding extent
vector deserves discussion. Using extent vector as per file log
undoubtedly enriches the semantics for CloudFS, however, memory
consumption in the meantime becomes unbounded, and only related to
revision history of a file. Think about overwriting the first one byte
for unlimited number of times, where the extent vector would expand
until memory or disk runs out.

Supporting Snapshots
====================

CloudFS snapshot provides a reliable way for backing up and restoring
file system states. It essentially takes copy of a minimum set of data
from which file system states at a given time could be inferred, and
keeps them safe by replicating remotely.

File System State and Anchor File
---------------------------------

CloudFS state consists of the following: Proxy file hierarchy, stored as
a directory tree on host file system; Segment objects stored locally;
Segment Mapping, Extent Mapping and Active File Table that are persisted
on unmount or as requested, and finally, the list of snapshots is also
part of CloudFS state. In order to build a self-contained snapshot
object, the list above is the minimum to copy.

All snapshot commands are issued through ioctl system call, with an
OpenedFile object pointer pointing to an anchor file. The anchor file is
a non-readable, non-writable and non-removable special file located at
CloudFS root directory. The only two operations allowed are opening with
read-only permission and issuing commands with ioctl. Ioctl carries two
arguments, respectively an integer valued command word and a pointer of
opaque type. Calls to ioctl are intercepted by FUSE and passed to the
entry point in our code.

Snapshots are uniquely identified by a 64-bit timestamp representing the
creation time. It is returned as a handler and passed as argument for
snapshot manipulation.

Taking and Restoring Snapshots
------------------------------

Snapshots are taken by sending command word to ioctl, and ioctl routes
the request to class $Snapshot$. After receiving the command, CloudFS
collects its state variables, including proxy files, segment objects and
disk dump of memory objects, into several tarballs, and then upload
tarballs to the cloud under a snapshot bucket. Buckets are assigned
self-descriptive names, like $snapshot\_ XXXXXXXX$, where $XXXXXXXX$ is
the timestamp. Using self-descriptive bucket names allows CloudFS to
recover the Snapshot List by scanning buckets on the cloud when the
local copy is missing.

Maintaining segment reference counts correct and consistent is crucial.
When snapshot is being taken, CloudFS increases reference counts for all
segments by one, implying that on later CloudFS operations these
segments will never be prematurely released because their reference
counts are at least one. Another important implication is that, for
restore operation, it is safe to decrease reference count segments
referred by current set of proxy files, and then simply overwrite
CloudFS states with those in the snapshot.

When snapshots are being restored, all snapshots taken after the target
needs to be deleted first. This is considered as a safety measure to
avoid jumping to the future and disappointedly finding out some segments
are missing, causing inconsistency. Deleting snapshot is merely a
reverse of taking one: Fetch Segment Map down from the cloud, decrease
reference count by one for every segment recorded in the map, and then
remove the snapshot bucket. Reference counts remain consistent after any
sequence of creation and deletion this way.

Installing and Uninstalling Snapshots
-------------------------------------

Installing a read-only image under the root directory for snapshots is
another interesting feature of CloudFS. An installed snapshot appears as
a normal read-only directory under the root named $snapshot\_ XXXXXXXX$,
with states of files and the directory tree being exactly the same as
those in the snapshot. Thanks to the flexibility of proxy files, segment
objects and reference counts, snapshot installation is no more than
downloading proxy tree from the cloud, and extracting the tarball under
current CloudFS root directory. Uninstalling a snapshot is no more than
removing its $snapshot\_ XXXXXXXX$ directory from CloudFS root.

The anchor file is entry point for installing and uninstalling
snapshots. User application issues the command word together with a
snapshot timestamp to be restored. After that, accessing to
$snapshot\_ XXXXXXXX$ directory is just normal file system operations.

FINE TUNING AND PARAMETERIZATION
================================

Previous sections already describe a working CloudFS design which has
been tested and proved correct on tens of test cases. However,
limitations exist for a prototype directly translated from the
specification above, and some of them become crucial under certain
conditions, rendering CloudFS unusable. Next we discuss several topics
related to CloudFS optimization and parameterization.

Garbage Collection and Metadata Explosion
-----------------------------------------

Resemblance to log-structured file system raises the same concern on
garbage collection (GC), and indeed, on an example given as overwriting
the first byte for unlimited number of times, metadata explodes while it
really should not with GC. Implementing the basic form of GC is trivial,
and decreasing reference counts for segments referred by a deleted file
suffices. Slightly more complicated (and useful) GC is triggered on
truncation, where a new extent vector is built by throwing away extents
starting after the truncation point. Truncating to zero is a special
case, equaling to remove the whole file and create a new one with the
same set of attributes.

Besides the basic form of GC, identifying extents made stale by
overwrites is helpful. An extent becomes stale when the range it covers
has been overshadowed by later writes. However, performing GC on each
write would induce intolerably high overhead and thus unfeasible.

A more proper GC strategy would be periodically scanning a selected set
of files, read their content, truncate to zero, and write back. This
implies performing log replay on each file, and compressing the log by
running deduplication. This process is much like “flatten” a overlaid
structure by pressing on the top.

Segment Caching
---------------

General purpose caching strategy is easily adopted into CloudFS as an
extra layer between segment management layer and object storage layer.
In CloudFS the benefits of caching are in two aspects. First, caching
objects from the cloud reduces costs for future requests on the same
object. Second, caching objects by delayed uploading helps coalescing
multiple uploads into one. Implementing per segment caching with single
user is easy: Given a cache size (1MB by default), cache every
download/upload until cache full, in which case use LRU to evict an item
after checking for DIRTY bit and proper handling of dirty items (write
back).

Reducing Cloud Cost
-------------------

Caching undoubtedly reduces cloud traffic and thus lowers cost. In
addition, other techniques can be adopted to further reduce cost by
cutting off necessary data transfers. We discuss some of them.

In snapshot restoration, avoiding downloading segment tarball is
possible, as long as every segment referred to by snapshot’s segment map
is present locally.

On file read, downloading stale segments could be avoided by replaying
the log locally first and filtering out stale segments. Similarly, on
file write, if we already know the write operation overshadows some
segments, removing them from the cloud would save cloud storage cost.

In snapshot creation, sometimes an incremental backup based on the
latest snapshot is better than full backup. However it requires to
accumulate snapshots earlier than the target for correct restoration. A
simpler approach would be to compress everything sent to the cloud.

Average Segment Size
--------------------

Trade-offs on average segment size affects cost and performance. Though
specific I/O pattern complicates analysis, but the general rule of thumb
is that, smaller segments favor deduplication because potentially
deduplication runs on a finer granularity. However, the inevitable
increase in matadata management cost, and cloud request cost offsets
part of the benefit. In contrast, larger segment size finds fewer
duplicated segments, but they reduce cloud request and management costs.

Evaluation
==========

CloudFS are tested on test cases in checkpoint 1, 2 and 3. Correctness
and robustness are obvious, since most tests are passed without any
special accommodation and patch over the specification. This justifies
the claim that our CloudFS implementation circumvents most of the corner
cases in deduplication compared with a non-log-structured design.
Furthermore, a strict layered architecture simplifies logic reasoning on
correct behavior.

Test cases for checkpoints 1 and 2 does not focus on cloud cost, and
therefore correctness explains everything. For checkpoint 3 cloud cost
and SSD cost are added as an important factor, providing an overview on
CloudFS more in-depth. In checkpoint 3, cloud cost is the largest
disadvantage, spending over \$180 while others only need less than \$70.
However, such cost discrepancy is expected, as the log-replay algorithm
and append-only immutable objects delay garbage collection, and
increases cloud traffic unnecessarily by reading stale segments. In
addition, caching and tarball compression is not implemented, which
further widens the gap.

As for SSD I/O traffic, there is no obvious deviation from what is
considered as normal. Sometimes CloudFS even performs better than a
traditional file system, not only because it deduplicates segments, but
also because it does not use disks as cache.