
#include "cloudfs.h"

off_t OnlineSegmentation::buffer_size = 16384;
int OnlineSegmentation::window_size = 48;
int OnlineSegmentation::avg_seg_size = 4096;
int OnlineSegmentation::min_seg_size = 2048;
int OnlineSegmentation::max_seg_size = 8192;

/*
 * Initialize parameters related to segmentation
 *
 * Set maximum seg size to be twice of the average
 * Set minimum seg size to be half of the average
 * Set buffer size to be twice of the maximum
 * Set rabin window size to 48
 */
void InitOnlineSegmentation(cloudfs_state *state)
{
	// [Min, Avg, Max] is a series of multiplier of 2
	state->min_seg_size = state->avg_seg_size >> 1;
	state->max_seg_size = state->avg_seg_size << 1;
	
	// rabin window size
	OnlineSegmentation::window_size = state->rabin_window_size;
	OnlineSegmentation::avg_seg_size = state->avg_seg_size;
	OnlineSegmentation::min_seg_size = state->min_seg_size;
	OnlineSegmentation::max_seg_size = state->max_seg_size;
	
	// Buffer size
	OnlineSegmentation::buffer_size = ((off_t)state->max_seg_size) << 1;
	
	dbg_printf("InitOnlineSegmentation(): [min, avg, max] seg size = "
			   "[%d, %d, %d]\n", 
			   state->min_seg_size,
			   state->avg_seg_size,
			   state->max_seg_size);
			   
	dbg_printf("InitOnlineSegmentation(): "
			   "OnlineSegmentation::buffer_size = "
			   "%lld\n", 
			   OnlineSegmentation::buffer_size);
	
	return;	
}

/*
 * CleanOnlineSegmentation() - Cleanup on unmount
 */
void CleanOnlineSegmentation()
{
	return;	
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

/*
 * Constructor - Initialize buffer and hash related variables
 */
OnlineSegmentation::OnlineSegmentation()
{
	this->buffer = (unsigned char *)malloc(sizeof(unsigned char) * \
					    OnlineSegmentation::buffer_size);
	dbg_assert(this->buffer);
	
	// There is 0 byte in the buffer at the beginning
	this->len = 0;
	
	// Initialize rabin polynomial
	// This needs to be created for every instance
	this->rabin_poly = \
		rabin_init(OnlineSegmentation::window_size, 
				   OnlineSegmentation::avg_seg_size, 
				   OnlineSegmentation::min_seg_size, 
				   OnlineSegmentation::max_seg_size);
				   
	// If initialization failed then assertion fail
	dbg_assert(this->rabin_poly);
	
	return;
}

/*
 * Destructor - Free up
 *
 * Free this->buffer
 * Free rabin poly
 */
OnlineSegmentation::~OnlineSegmentation()
{
	dbg_assert(this->buffer);
	 
	free(this->buffer);
	rabin_free(&this->rabin_poly);
	
	return;
}

/*
 * StoreSegment() - Pack buffer data into segment object
 *
 * Returns the segment ID of newly created or existing segment
 */
seg_id_t OnlineSegmentation::StoreSegment(void *data, 
										  off_t length,
										  SegmentPlacementHint sph)
{	
	Segment *seg_p = Segment::PutData(data, length, sph);
	
	return seg_p->id;
}

/*
 * AppendData() - Append data for deduplication
 *
 * It keeps dedup for a consecutive stripe of data, until 
 * the writing pattern is over. There would be data cached in
 * memory after each write(), therefore remember to seal the
 * buffer everytime write of data stripe is over.
 *
 * Return value is an array of segment IDs we have created for
 * the current stripe of data. It could be empty, as no segment
 * boundary is available currently. But eventually on call to 
 * Seal() there will be some data (but Seal() does not always 
 * return a segment, also)
 */
vector<WriteObject> OnlineSegmentation::AppendData(const void *data, 
												   off_t length,
												   SegmentPlacementHint sph)
{
	// Holds all segment IDs we have created
	vector<WriteObject> write_obj_list;
	write_obj_list.clear();
	
	unsigned char *data_ptr = (unsigned char *)data;
	// It should be while(new_seg_flag == 1)
	// but we just do break at the end for readability
	while(1)
	{
		// To make sure buffer_cap > 0
		dbg_assert(OnlineSegmentation::buffer_size > this->len);
		// Buffer capacity
		off_t buffer_cap = OnlineSegmentation::buffer_size - this->len;
		off_t copy_len = (buffer_cap > length) ? length : buffer_cap;
		
		memcpy(this->buffer + this->len, 
			   data_ptr, 
			   copy_len);
			   
		data_ptr += copy_len;
		// Current length of the buffer
		this->len += copy_len;
		
		// Even if length goes down to 0, we could still do multiple
		// rounds of segmentation, since the remaining bytes in buffer
		// might have one or more boundaries
		length -= copy_len;
		
		off_t seg_len;
		int new_seg_flag = 0;
		
		seg_len = rabin_segment_next(this->rabin_poly, 
								     (const char *)this->buffer, 
									 this->len, 
									 &new_seg_flag);
		if(new_seg_flag == 1)
		{
			seg_id_t seg_id = this->StoreSegment(this->buffer, 
											     seg_len,
												 sph);
			// Add a write object
			WriteObject new_wo;
			new_wo.size = seg_len;
			new_wo.id = seg_id;
			write_obj_list.push_back(new_wo);
				
			this->len -= seg_len;
			// Copy overlapping memory
			// Shift contents by seg_len bytes
			// And in next iteration we refill them
			memcpy(this->buffer, 
				   this->buffer + seg_len, 
				   this->len);
			dbg_printf("AppendData(): Found new segment length %lld\n",
					   seg_len);
		} else {
			dbg_printf("AppendData(): End of buffer, remain size %lld\n",
					   this->len);
			break;
		}
	}
	
	// Return the list of segment IDs
	return write_obj_list;
}

/*
 * Seal() - Create a segment based on what's left in the buffer
 *
 * Corner case is that if the buffer is empty, calling Seal()
 * would have no effect and return Segment::segment_id_invalid
 * from argument
 */
bool OnlineSegmentation::Seal(WriteObject *wo,
							  SegmentPlacementHint sph)
{
	if(this->len > 0)
	{
		// It returns new segment ID no matter the segment 
		// previously exists or not (and take care of segment 
		// destruction issue)
		wo->id = this->StoreSegment(this->buffer, 
									this->len,
									sph);
									
		wo->size = this->len;
		
		// DO NOT FORGET THIS!!!!!!!!!!!!!!!!!!!!!
		// Because we flush all data
		this->len = 0;
		
		return true;
	}
	
	dbg_printf("Seal(): Buffer empty, no data need to be sealed\n");
	wo->id = Segment::segment_id_invalid;
	wo->size = 0;

	return false;
}
