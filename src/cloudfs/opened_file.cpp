
#include "cloudfs.h" 

unordered_map<FileHandler, OpenedFile *, FileHandlerHasher> ActiveFileMap;

int OpenedFile::size_threshold = 64 * 1024;

// This is the default permission for an opened file
// File system level permission check has already filtered out
// those unqualified file for certain operation
// These bits are defined to impose logic permissions
IOPermission OpenedFile::all_permission = \
	IOPermission{true, true, true, true, true, 
				 false, // IOCTL
				 };

void InitOpenedFile(cloudfs_state *state)
{
	state = state;
	
	ActiveFileMap.clear();
	OpenedFile::size_threshold = state->threshold;
	
	return;
}

void CleanOpenedFile()
{
	return;	
}

/*
 * Constructor - Initialize variables
 *
 * Copy path
 * Add itself into opened file table
 * Set prev_write_flag to false
 * Initialize proxy file (attr caching)
 * Initialize online segmentation
 * Initialize reference count
 */
OpenedFile::OpenedFile(const char *ppath, 
					   IOPermission pperm)
{
	this->fh = FileHandler(ppath);
	// By default file permission is R/W/T/C/U
	this->permission = pperm;
	
	// Make sure file not opened
	dbg_assert(!OpenedFile::FileOpened(ppath));
	ActiveFileMap[this->fh] = this;
	
	this->prev_write_flag = false;
	this->prev_write_start = 0;
	this->prev_write_len = 0;
	this->prev_write_cached = 0;
	
	this->proxy_file = new ProxyFile(ppath, pperm.ioctl);
	this->online_seg = new OnlineSegmentation();
	
	// We know proxy file must exist
	// because FUSE will call mknod before open()
	// and in mknod we create proxy file structure
	// and write initial stat into the proxy and dump it
	this->proxy_file->ReadAll();
	
	// This is incremented if we open a file and found
	// the file is already open.
	// Decreased if we close a file. Deleted if this drops
	// to 0
	this->ref_count = 1;
	
	if(this->proxy_file->sph.placement == SegmentPlacementHint::upload_this)
	{
		dbg_printf("OpenedFile::OpenedFile():"
				   " All operation goes into cloud\n");
	}
	
	return;	
}

/*
 * Destructor
 *
 * Write back segment changes (and also updated stat)
 * Destroy proxy file
 * Destory online segmentation
 * Delete active file entry
 */
OpenedFile::~OpenedFile()
{
	// Make sure the file is not opened (i.e. not in the map)
	// This means we did not Close() properly
	if(OpenedFile::FileOpened(this->fh.path))
	{
		dbg_printf("OpenedFile::~OpenedFile(): WARNING: File not closed!\n");
		ActiveFileMap.erase(this->fh);
		
		// Since no Close() is called, we need to commit
		// changes to proxy file
		this->proxy_file->SerializeAll();
	}
	
	delete this->proxy_file;
	delete this->online_seg;
	
	return;
}

/*
 * OpenedFile::FileOpened() - Check whether a file is already opened
 *
 * Returns true if yes, no otherwise.
 * It checks whether the path already exists in the map
 */
bool OpenedFile::FileOpened(const char *path)
{
	return ActiveFileMap.find(FileHandler(path)) != ActiveFileMap.end();
}

/*
 * OpenedFile::GetOpenedFile() - If a file is opened, return a pointer
 * to the structure
 *
 * It requires the file being contained in the map
 */
OpenedFile *OpenedFile::GetOpenedFile(const char *path)
{
	dbg_assert(OpenedFile::FileOpened(path));
	
	FileHandler h = FileHandler(path);
	
	return ActiveFileMap[h];
}

/*
 * GetProxyFile() - Return proxy file if the file is already opened
 *
 * Otherwise construct a proxy file and return to user
 * NOTE: (1) Please delete it before exit
 *       (2) For Read, Writre, Close, Truncate do not use this
 */
ProxyFile *OpenedFile::GetProxyFile(const char *path)
{	
	if(OpenedFile::FileOpened(path))
	{
		ProxyFile *pf = OpenedFile::GetOpenedFile(path)->proxy_file;
		// First persist what we have done
		pf->SerializeStat();
		
		return pf;
	}
	
	return new ProxyFile(path);
}

/*
 * SealCachedExtent() - Force a segment boundary on current write cache
 *
 * The current write cahce this->online_seg->buffer stores part of data
 * written by previous Write() calls in which there is no natural fingerprint
 * boundary. However, some operation requires this piece of data being
 * forced to disk (i.e. POSIX read()) and thus we generate an artificial
 * boundary. 
 */
void OpenedFile::SealCachedExtent()
{
	WriteObject wo;
	
	this->online_seg->Seal(&wo, this->proxy_file->sph);
	if(wo.size == 0)
	{
		dbg_printf("OpenedFile::SealCachedExtent(): No content to seal\n");
		return;	
	}
	
	Extent *ext_p = new(Extent);
	ext_p->id = wo.id;
	
	// Size already persisted to disk
	off_t committed_len = this->prev_write_len - wo.size;
	// Start of cached data
	off_t cached_start = this->prev_write_start + committed_len;
	
	ext_p->start = cached_start;
	this->proxy_file->journal.push_back(ext_p);
	
	// This also counts as write!
	this->prev_write_start = cached_start;
	this->prev_write_len = wo.size;
	this->prev_write_cached = 0;
	
	dbg_printf("OpenedFile::SealCachedExtent(): Sealed %lld sized extent\n",
			   wo.size);
	
	return;
}


/*
 * KeepSequentialWrite() - Records write information and force seal
 *
 * This function is called on every Write() operation. It (1) records
 * current write parameter for future use, and (2) Check whether we
 * are writing sequentially. If we just switch from other operations, 
 * or we are writing sequentially, then there is no need to force
 * seal. Otherwise, seal() is issued and a new extent (hopefully)
 * is added to journal.
 */
void OpenedFile::KeepSequentialWrite(off_t start, off_t end)
{
	dbg_assert(end >= start);
	off_t prev_write_end = this->prev_write_start + this->prev_write_len - 1;
	
	// If previous operation is not write, just record 
	// current write, and no seal()
	if(this->prev_write_flag == false) 
	{
		this->prev_write_start = start;
		this->prev_write_len = end - start + 1;	
		this->prev_write_flag = true;
		return;	
	}
	
	this->prev_write_flag = true;
	if(prev_write_end + 1 == start)
	{
		this->prev_write_start = start;
		this->prev_write_len = end - start + 1;
		// Sequential write - no seal()
		dbg_printf("OpenedFile::KeepSequentialWrite(): Seq write!"
				   " prev_end = %lld\n", prev_write_end);
		return;
	}
	
	// Non sequential write - seal()
	dbg_printf("OpenedFile::KeepSequentialWrite(): Non-seq write!"
			   " prev_end = %lld, start = %lld\n",
			   prev_write_end,
			   start);
	
	// Must do this before any modification to this->prev_###
	// because it makes use of these variables to deal with
	// the last extent size
	this->SealCachedExtent();
	
	this->prev_write_start = start;
	this->prev_write_len = end - start + 1;
	
	return;
}

/*
 * SwitchFromWrite() - Current operation is not Write()
 *
 * Unsets prev_write_flag and seals previously cached write data
 * to force consistency (e.g. Read() immediately after Write())
 */
void OpenedFile::SwitchFromWrite()
{
	// Previous is not write()
	if(this->prev_write_flag == false) 
		return;	
	
	dbg_printf("OpenedFile::SwitchFromWrite(): Data cached, flush buffer\n");
	this->SealCachedExtent();
	
	this->prev_write_flag = false;
	
	return;
}

/*
 * Read() - Read deduped file
 *
 * Reads deduplicated file using log replay.
 * Write history is recorded in the journal as extents
 * when reading a range, just check each extent and apply
 * them to the buffer. ApplyTo() function of Extent
 * automatically deals with data retrivial and intersection
 *
 * Read() operation seals a segment currently being cached
 */
size_t OpenedFile::Read(void *buffer, off_t start, size_t size)
{
	// Force flush cached data
	this->SwitchFromWrite();
	
	// Deals with holes
	memset(buffer, 0, size);
	
	// The last byte offset of the file
	off_t max_offset = this->proxy_file->stat_obj.st_size - 1;

	// If read exceeds EOF then just return 0 byte
	if(start > (off_t)max_offset) 
		return 0;

	// The last requested byte offset
	off_t end = start + (off_t)size - 1;
	
	// The less offset becomes true read end
	off_t read_end = (max_offset > end) ? end : max_offset;
	// If read start is within the file then just copy
	off_t read_start = start;
	
	dbg_printf("OpenedFile::Read(): [%lld, %lld]\n", read_start, read_end);
	
	int num_extent = this->proxy_file->journal.size();
	dbg_printf("OpenedFile::Read(): Replaying %d logs\n", num_extent);
	
	for(int i = 0;i < num_extent;i++)
	{
		Extent *ext_p = this->proxy_file->journal[i];
		// Apply the extent to [read_start, read_end]
		ext_p->ApplyTo(read_start, read_end, buffer);	
	}
	
	return read_end - read_start + 1;
}

/*
 * Write() - Append log to the end of proxy file
 *
 * If a write operation extends the length of the file, then
 * st_size in proxy is also updated. 
 *
 * On each write operation, must check whether there is a previous write
 * and whether these two writes are sequential. If not, then we need to
 * force sealing the previous write before conducting current one.
 *
 * If the last recent operation is not a write() then do not need to check
 * for write sequentiality
 */
size_t OpenedFile::Write(const void *buffer, off_t start, size_t size)
{	
	off_t end = start + (off_t)size - 1;
	// If is sequential write just update write information
	// If is not sequential write, seal previously cached data
	// If first Write() after other operation just update 
	// write information
	this->KeepSequentialWrite(start, end);
	
	off_t file_end = (off_t)this->proxy_file->stat_obj.st_size - 1;
	if(file_end < end)
	{
		dbg_printf("OpenedFile::Write(): Extend file size from %lld to %lld\n",
				   file_end + 1,
				   end + 1);
		this->proxy_file->stat_obj.st_size = end + 1;	
	}
	
	if(file_end + 1 <= OpenedFile::size_threshold && 
	   end + 1 > OpenedFile::size_threshold)
	{
		dbg_printf("OpenedFile::Write(): Just crossed threshold. "
		           "Switch to remote mode\n");
		           
		this->proxy_file->sph.placement = \
			SegmentPlacementHint::upload_this;
		
		// Upload all segments to cloud
		Segment::UploadDataInJournalToCloud(&this->proxy_file->journal);
	}
	
	vector<WriteObject> write_obj_list = \
		this->online_seg->AppendData(buffer, size, this->proxy_file->sph);
	
	int num_extent = write_obj_list.size();
	dbg_printf("OpenedFile::Write(): Appending %d logs\n", num_extent);
	
	// Start of each individual extent
	off_t extent_start = start - this->prev_write_cached;
	
	for(int i = 0;i < num_extent;i++)
	{
		Extent *ext_p = new(Extent);
		ext_p->id = write_obj_list[i].id;
		ext_p->start = extent_start;
		
		dbg_printf("OpenedFile::Write(): [start, end] = [%lld, %lld]\n",
				   extent_start,
				   extent_start + write_obj_list[i].size - 1);
		
		extent_start += write_obj_list[i].size;
		
		this->proxy_file->journal.push_back(ext_p);	
	}
	
	off_t last_byte_no_cache = start + (off_t)size; //- 1;
	off_t last_byte_actual = extent_start; //- 1;
	
	this->prev_write_cached = last_byte_no_cache - last_byte_actual;
	
	return size;
}

/*
 * Close() - Close an opened file (no reference count)
 *
 * Flushes pending write cache, and remove the entry
 */
void OpenedFile::Close()
{
	dbg_printf("OpenedFile::Close()\n");
	
	// Force flush before Close()
	this->SwitchFromWrite();
	this->proxy_file->SerializeAll();
	
	// Make sure the file is opened
	dbg_assert(ActiveFileMap.find(this->fh) != ActiveFileMap.end());
	
	ActiveFileMap.erase(this->fh);
	
	return;
}

/*
 * Truncate() - Truncate an opened file
 *
 * This provides an interface for truncating an opened file
 * because if a file is active then we might have to flush the 
 * write cache before truncating it.
 *
 * To truncate a closed file, just call Truncate() method in
 * ProxyFile class (but you have to initialize that manually)
 */
void OpenedFile::Truncate(off_t new_size)
{
	this->SwitchFromWrite();
	
	dbg_printf("OpenedFile::Truncate(): %lld\n", new_size);
	
	this->proxy_file->Truncate(new_size);
	
	return;
}

/*
 * Delete() - Wrapper for proxy_file Delete()
 *
 * Switches from write mode to other mode
 */
void OpenedFile::Delete()
{
	this->SwitchFromWrite();
	
	dbg_printf("OpenedFile::Delete(): %s\n", 
			   this->proxy_file->full_path);
	
	this->proxy_file->Delete();
	
	return;
}
