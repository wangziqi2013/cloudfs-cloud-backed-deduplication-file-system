
#include "cloudfs.h"

char Snapshot::full_path[MAX_PATH_LEN];
// Used for full path construction
const char Snapshot::snapshot_filename[] = ".snapshot";
// Used for path comparison (not in full path construction)
const char Snapshot::snapshot_fuse_path[] = "/.snapshot";
// Used for metadata uploading
const char Snapshot::snapshot_disk_filename[] = ".snapshot.list";
// Used for segment&proxy uploading
const char Snapshot::snapshot_proxy_filename[] = "proxy.tar";
const char Snapshot::snapshot_segment_filename[] = "segment.tar";

// Stores all snapshots we heve generated within this system
vector<Snapshot> SnapshotList;

int cloudfs_unlink(const char *path);
int cloudfs_rmdir(const char *path);


/*
 * InitSnapshot() - Initialize a snapshot anchor file
 *
 * Copy snapshot path information
 * Create anchor file if does not exist
 * Load segment from disk if exists
 */
void InitSnapshot(cloudfs_state *state)
{
	int anchor_fd;
	
	strcpy(Snapshot::full_path, ProxyFile::base_path);
	// ...../proxy/
	strcat(Snapshot::full_path, ProxyFile::proxy_dir);
	// ...../proxy/.snapshot
	strcat(Snapshot::full_path, Snapshot::snapshot_filename);
	
	dbg_printf("InitSnapshot(): Anchor located @ %s\n", 
			   Snapshot::full_path);
	
	// Create a snapshot file: No any permission
	anchor_fd = open(Snapshot::full_path,
			 	     O_CREAT | O_EXCL,
			   		 S_IRUSR | S_IRGRP | S_IROTH); // 0444 - Read only
			   		 
	if(anchor_fd < 0)
	{
		if(errno == EEXIST)
		{
			dbg_printf("InitSnapshot(): Found snapshot anchor!\n");
		}
		else
		{
			dbg_printf("InitSNapshot(): Create error: %s\n", strerror(errno));
			dbg_assert(0);	
		}
	}
	else
	{
		close(anchor_fd);	
	}
	
	Snapshot::LoadSnapshotList();
		
	return;
}

/*
 *
 */
void CleanSnapshot()
{
	Snapshot::SaveSnapshotList();
	return;
}

/*
 * GetSnapshotTimestamp() - Return a 64 bit timestamp
 *
 * The precision of the timestamp is microseconds.
 * So the interval between two operations should not exceed
 * the precision allowed
 */
snapshot_ts_t Snapshot::GetSnapshotTimestamp()
{
	struct timeval tv;
	
	// The second argument is usually not used
	gettimeofday(&tv, NULL);
	
	// The time stamp is calculated by calulating microseconds
	snapshot_ts_t ts = (snapshot_ts_t)tv.tv_sec * (snapshot_ts_t)1000000 + \
				       (snapshot_ts_t)tv.tv_usec;
				    
	return ts;
}

/*
 * GetSnapshotListDiskFilename() - Return the file name of snapshot list
 *
 * This list is persisted to the disk on unmount, and loaded into memory
 * on file system mount
 */
char *Snapshot::GetSnapshotListDiskFilename()
{
	static char full_path[MAX_PATH_LEN];
	
	strcpy(full_path, ProxyFile::base_path);
	strcat(full_path, Snapshot::snapshot_disk_filename);
	
	return full_path;	
}

/*
 * Snapshot::SaveSnapshotList() - Persist snapshot list to file
 */
void Snapshot::SaveSnapshotList()
{
	char *file_path = Snapshot::GetSnapshotListDiskFilename();
	int list_size = SnapshotList.size();
	int ret;
	
	int fd = open(file_path, 
			      O_CREAT | O_WRONLY | O_TRUNC,
				  S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
	dbg_assert(fd > 0);
	
	ret = write(fd, &list_size, sizeof(list_size));
	dbg_assert(ret == sizeof(list_size));
	
	for(int i = 0;i < list_size;i++)
	{	
		// Copy-construct a snapshot
		Snapshot sn = SnapshotList[i];
		
		ret = write(fd, &sn, sizeof(sn));
		dbg_assert(ret == sizeof(sn));
	}
	
	dbg_assert(close(fd) == 0);
	
	return;
}

/*
 * get_bucket_callback() - Called by cloud API 
 */
int cloud_list_service_callback(const char *key)
{
	if(strlen(key) < 9)
		return 0;
	
	// Hold "snapshot_\0"
	char buffer[10];
	memcpy(buffer, key, 10);
	buffer[9] = '\0';
	if(strcmp(buffer, "snapshot_") == 0)
	{
		char *ts_str = (char *)key + 9;	
		snapshot_ts_t ts;
		unsigned long temp, temp2;
		
		dbg_assert(sscanf(ts_str, "%lx", &temp) == 1);
		dbg_assert(sscanf(ts_str + 8, "%lx", &temp2) == 1);
		ts = (((unsigned long long)temp) << 32) | (unsigned long long)temp2;
		dbg_assert(sscanf(ts_str, "%llx", &ts) == 1);
		
		Snapshot ss;
		ss.timestamp = ts;
		ss.installed = 0;
		
		dbg_printf("get_bucket_callback(): Push snapshot_%.16llX\n",
				   ts);
		
		SnapshotList.push_back(ss);
	}
	
	return 0;	
}

/*
 * RebuildSnapshotListFromCloud() - As name suggests
 *
 * If we do not find a snapshot list file locally then
 * we try to rebuild one from the cloud.
 */
void Snapshot::RebuildSnapshotListFromCloud()
{
	cloud_list_service(cloud_list_service_callback);
	cloud_print_error();
	
	return;
}


/*
 * LoadSnapshotList() - Load the snapshot list from file
 *
 * If the file does not exist initialize list to be empty
 * If exists load using the following format:
 *
 * list_size - sizeof(int)
 * timestamp - sizeof(snapshot_ts_t) * list_size
 *
 */
void Snapshot::LoadSnapshotList()
{
	char *file_path = Snapshot::GetSnapshotListDiskFilename();
	int ret;
	int list_size;
	
	int fd = open(file_path,
				  O_RDONLY);
	if(fd < 0)
	{
		dbg_printf("Snapshot::LoadSnapShotList(): File not found.\n");
		SnapshotList.clear();
		
		Snapshot::RebuildSnapshotListFromCloud();
		
		return;	
	}
	
	ret = read(fd, &list_size, sizeof(list_size));
	dbg_assert(ret == sizeof(list_size));
	
	dbg_printf("Snapshot()::LoadSnapshotList(): Load %d snapshots\n", 
			   list_size);
	
	for(int i = 0;i < list_size;i++)
	{
		Snapshot sn;
		
		ret = read(fd, &sn, sizeof(sn));
		dbg_assert(ret == sizeof(sn));	
		
		SnapshotList.push_back(sn);
	}
	
	dbg_assert(close(fd) == 0);
	
	return;
}

/*
 * GetSnapshotBucketName() - Get the bucket name for a certain snapshot
 *
 * The name is 26 characters long, snapshot_xxxxxxxxxxxxxxxx\0
 * Buckets are created when uploading snapshots to the cloud, and 
 * removed when deleting snapshots
 */
char *Snapshot::GetSnapshotBucketName(snapshot_ts_t ts)
{
	// snapshot_xxxxxxxxxxxxxxxx\0 = 16 + 9 + 1 = 26
	static char name[Snapshot::snapshot_bucket_name_len];
	
	sprintf(name, "snapshot_%.16llX", ts);
	
	return name;
}

/*
 * TarProxyAndSegment() - Tar proxy files and segments on the local
 * as a single tar file
 *
 * The file name is: ssd_path/proxy_segment.tar
 */
void Snapshot::TarProxyAndSegment()
{
	char target_name[MAX_PATH_LEN];
	char proxy_path[MAX_PATH_LEN];
	char segment_path[MAX_PATH_LEN];
	
	strcpy(target_name, ProxyFile::base_path);
	strcat(target_name, Snapshot::snapshot_proxy_filename);
	
	strcpy(proxy_path, ProxyFile::base_path);
	strcat(proxy_path, ProxyFile::proxy_dir);
	
	TAR *tar_p;
	int ret;
	
	ret = tar_open(&tar_p, 
				   target_name, 
				   NULL, 
				   O_WRONLY | O_CREAT, 
				   0644, 
				   TAR_GNU);
	dbg_assert(ret == 0);
	
	DIR *dir;
	struct dirent *entry;
	
	dir = opendir(proxy_path);
	dbg_assert(dir != NULL);
	
	while((entry = readdir(dir)) != NULL)
	{
		if(strcmp(entry->d_name, Snapshot::snapshot_filename) == 0)
		{
			dbg_printf("Snapshot::TarProxyAndSegment(): Ignore .snapshot\n");
			continue;
		}
		else if(strcmp(entry->d_name, ".") == 0 ||
				strcmp(entry->d_name, "..") == 0)
		{
			dbg_printf("Snapshot::TarProxyAndSegment(): Ignore . and ..\n");
			continue;	
		}
		
		char full_path[MAX_PATH_LEN];
		strcpy(full_path, proxy_path);
		strcat(full_path, entry->d_name);
		
		struct stat stat_obj;
		dbg_assert(lstat(full_path, &stat_obj) == 0);
		
		if(strlen(entry->d_name) > 9)
		{
			char buffer[10];
			memcpy(buffer, entry->d_name, 10);
			buffer[9] = '\0';
			
			// If we are looking at a directory 
			// and its filename begins with snapshot_
			// just skip it
			if(S_ISDIR(stat_obj.st_mode) &&
			   strcmp(buffer, "snapshot_") == 0)
			{
				continue;
			}
		}
		
		if(S_ISDIR(stat_obj.st_mode))
		{
			dbg_printf("Snapshot::TarProxyAndSegment(): adding dir %s\n",
					   full_path);
			ret = tar_append_tree(tar_p, full_path, entry->d_name);	
			dbg_assert(ret == 0);
		}
		else
		{
			dbg_printf("Snapshot::TarProxyAndSegment(): adding file %s\n",
					   full_path);
			ret = tar_append_file(tar_p, full_path, entry->d_name);
			dbg_assert(ret == 0);
		}
	}
	
	dbg_assert(closedir(dir) == 0);
	
	ret = tar_append_eof(tar_p);
	dbg_assert(ret == 0);
	
	ret = tar_close(tar_p);
	dbg_assert(ret == 0);
	
	// Then do the same
	
	strcpy(target_name, ProxyFile::base_path);
	strcat(target_name, Snapshot::snapshot_segment_filename);
	
	strcpy(segment_path, ProxyFile::base_path);
	strcat(segment_path, Segment::seg_local_dir);
	
	ret = tar_open(&tar_p, 
				   target_name, 
				   NULL, 
				   O_WRONLY | O_CREAT, 
				   0644, 
				   TAR_GNU);
	dbg_assert(ret == 0);
	
	ret = tar_append_tree(tar_p,
						  segment_path,
						  (char *)Segment::seg_local_dir);
	dbg_assert(ret == 0);
	
	ret = tar_append_eof(tar_p);
	dbg_assert(ret == 0);
	
	ret = tar_close(tar_p);
	dbg_assert(ret == 0);
	
	return;
}

/*
 * put_fd - A global file descriptor for the file being transferred
 * to the cloud
 *
 * cloud_put_callback() - As name suggests
 */
static int put_fd;
static int cloud_put_callback(char *buffer, int len)
{	
	int read_len = read(put_fd, buffer, len);
			  
	return read_len;
}

/*
 * SendFileToCloud() - Send a local disk file to the cloud
 *
 * Sends file pointed by path to cloud with bucket name bucket
 * If bucket does not exist create the bucket first
 * If bucket is given NULL then does not create the bucket
 */
void Snapshot::SendFileToCloud(const char *path, 
							   const char *bucket,
							   const char *name)
{ 	
  	dbg_printf("Snapshot::SendFileToCloud(): %s -> %s/%s\n",
	  		   path,
			   bucket,
			   name);
  	
  	// Open and assign to the globally visible fd
  	put_fd = open(path, O_RDONLY);
  	dbg_assert(put_fd >= 0);
  	
  	struct stat stat_obj;
  	dbg_assert(lstat(path, &stat_obj) == 0);
  	
  	cloud_put_object(bucket,  // Bucket name
					 name,    // Name inside bucket
					 stat_obj.st_size,
					 cloud_put_callback);
	cloud_print_error();
					 
	dbg_assert(close(put_fd) == 0);
	
	dbg_printf("Snapshot::SendFileToCloud(): Finish\n");
	
	return;
}


/*
 * get_fd - A globally visible file descritor to store file form cloud
 *
 * cloud_get_callback() - Call back called by cloud API to write
 * chunks into a given file
 */
static int get_fd;
static int cloud_get_callback(const char *buffer, int len)
{
	return write(get_fd, buffer, len);
}

/*
 * GetFileFromCloud() - Download a file from cloud
 */
void Snapshot::GetFileFromCloud(const char *path,
							    const char *bucket,
								const char *name)
{
	dbg_printf("Snapshot::GetFileFromCloud(): %s/%s -> %s\n",
			   bucket,
			   name,
			   path);
	// Open a local file, truncate if exists, create if not exist
	get_fd = open(path, 
				  O_CREAT | O_WRONLY | O_TRUNC,
				  S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
	dbg_assert(get_fd >= 0);
	
	cloud_get_object(bucket, 
					 name,
                     cloud_get_callback);
	cloud_print_error();
	
	dbg_assert(close(get_fd) == 0);
	
	return;	
}

/*
 * IncreaseAllRefcountByOne() - Increase the reference count of all segments
 * in the current segment map by 1
 */
void Snapshot::IncreaseAllRefcountByOne()
{
	seg_map_it it;
	
	for(it = SegmentMap.begin();it != SegmentMap.end();it++)
	{
		Segment *seg_p = it->second;
		
		seg_p->ref_count++;	
	}
	
	return;
}

/*
 * DecreaseSomeRefcountByOne() - Decrease the reference of some segments
 * by 1
 *
 * These segments are given in the hash object. Decrease reference count
 * is equivalent to unlink segment
 */
void Snapshot::DecreaseSomeRefCountByOne(vector<HashObject> seg_list)
{
	int seg_num = seg_list.size();
	
	dbg_printf("Snapshot::DecreaseSomeRefcountByOne(): %d segments\n",
			   seg_num);
	
	for(int i = 0;i < seg_num;i++)
	{
		Segment *seg_p = SegmentMap[seg_list[i]];
		
		// It might case segment removal, or not
		seg_p->Unlink();
	}
	
	return;
}

/*
 * TakeSnapshot() - This call takes a snapshot and upload it to the cloud
 *
 * The process goes as follows:
 * (1) Get timestamp
 * (2) Create bucket using the timestamp
 * (3) Tar proxy&segment
 * (4) Dump metadata (seg map, ext map, snapshot list)
 * (5) Send metadata to cloud
 * (6) Send proxy to cloud
 * (7) Increase seg refcount by 1
 * (8) Add snapshot object to the list
 * (9) Remove proxy&segment tarball 
 */
snapshot_ts_t Snapshot::TakeSnapshot()
{
	char path[MAX_PATH_LEN];
	
	// Get current time as timestamp
	snapshot_ts_t ts = Snapshot::GetSnapshotTimestamp();
	
	char *bucket_name = Snapshot::GetSnapshotBucketName(ts);
	dbg_printf("Snapshot::TakeSnapshot(): Create bucket %s\n", bucket_name);
	cloud_create_bucket(bucket_name);
	cloud_print_error();
	
	Snapshot::TarProxyAndSegment();
	
	// Upload segment for proxy tar ball
	strcpy(path, ProxyFile::base_path);
	strcat(path, Snapshot::snapshot_proxy_filename);
	dbg_printf("Snapshot::TakeSnapshot(): Upload proxy %s\n", path);
	Snapshot::SendFileToCloud(path, 
							  bucket_name, 
							  Snapshot::snapshot_proxy_filename);
	
	dbg_printf("Snapshot::TakeSnapshot(): Delete temp file\n");
	dbg_assert(unlink(path) == 0);
	
	// Do the same for segment
	strcpy(path, ProxyFile::base_path);
	strcat(path, Snapshot::snapshot_segment_filename);
	dbg_printf("Snapshot::TakeSnapshot(): Upload segment %s\n", path);
	Snapshot::SendFileToCloud(path, 
							  bucket_name, 
							  Snapshot::snapshot_segment_filename);
	
	dbg_printf("Snapshot::TakeSnapshot(): Delete temp file\n");
	dbg_assert(unlink(path) == 0);
	
	dbg_printf("Snapshot::TakeSnapshot(): Add object to list\n");
	
	Snapshot sn;
	sn.installed = 0;
	sn.timestamp = ts;
	SnapshotList.push_back(sn);
	
	Snapshot::SaveSnapshotList();
	
	strcpy(path, ProxyFile::base_path);
	strcat(path, Snapshot::snapshot_disk_filename);
	dbg_printf("Snapshot::TakeSnapshot(): Upload snapshot list %s\n", path);
	Snapshot::SendFileToCloud(path,
						      bucket_name,
							  Snapshot::snapshot_disk_filename);
	
	Segment::SaveSegmentMap();
	
	strcpy(path, ProxyFile::base_path);
	strcat(path, Segment::seg_map_filename);
	dbg_printf("Snapshot::TakeSnapshot(): Upload segment map %s\n", path);
	Snapshot::SendFileToCloud(path,
						      bucket_name,
							  Segment::seg_map_filename);
	
	Segment::SaveExtentMap();
							  
	strcpy(path, ProxyFile::base_path);
	strcat(path, Segment::ext_map_filename);
	dbg_printf("Snapshot::TakeSnapshot(): Upload extent map %s\n", path);
	Snapshot::SendFileToCloud(path,
						      bucket_name,
							  Segment::ext_map_filename);
	
	dbg_printf("Snapshot::TakeSnapshot(): Increase ref count by 1\n");
	Snapshot::IncreaseAllRefcountByOne();
	
	dbg_printf("Snapshot::TakeSnapshot(): Finished\n");
	
	return ts;
}

/*
 * IsSnapshotFolderName() - Judge whether a directory name is
 * a installed snapshot
 */
bool Snapshot::IsSnapshotFolderName(char *dname)
{
	// That;s not magic number: strlen("snapshot_") + 1
	char buffer[10];
	
	// Avoid buffer overflow
	if(strlen(dname) < 9)
		return false;
	
	memcpy(buffer, dname, 9);
	buffer[9] = '\0';
	
	return strcmp(buffer, "snapshot_") == 0;
}

/*
 * PurgeUserData() - Remove all user data under /base_path/proxy
 *
 * With the following exceptions:
 * (1) . and ..
 * (2) Anything that is a installed version of snapshot 
 */
void Snapshot::PurgeUserData(char *start, bool do_unlink)
{
	// This is 4KB for a call, and we do this recursively
	// That might be dangerous.......
	char path[MAX_PATH_LEN];
	DIR *dir;
	struct dirent *entry;
	
	strcpy(path, ProxyFile::base_path);
	strcat(path, ProxyFile::proxy_dir);
	// Start is "" means start from /base_path/proxy/
	// Do not append '/' to the end
	strcat(path, start);
	
	dir = opendir(path);
	dbg_assert(dir != NULL);
	
	int counter = 0;
	
	while((entry = readdir(dir)) != NULL)
	{
		if(counter++ > 100) break;
		
		dbg_printf("Snapshot::PurgeUserData(): d_name = %s\n", 
				   entry->d_name);
				   
		if(strcmp(entry->d_name, ".") == 0 || 
		   strcmp(entry->d_name, "..") == 0 ||
		   strcmp(entry->d_name, Snapshot::snapshot_filename) == 0)
		{
			dbg_printf("Snapshot::PurgeUserData(): %s Ignored\n",
					   entry->d_name);
					   
			continue;	
		}
		
		strcpy(path, ProxyFile::base_path);
		strcat(path, ProxyFile::proxy_dir);
		strcat(path, start);
		strcat(path, entry->d_name);
		
		dbg_printf("Snapshot::PurgeUserData(): path = %s\n", path);
		
		struct stat stat_obj;
		dbg_assert(lstat(path, &stat_obj) == 0);
		
		if(S_ISDIR(stat_obj.st_mode)) 
		{
			strcpy(path, "/");
			strcat(path, start);
			strcat(path, entry->d_name);
			strcat(path, "/");
			
			if(Snapshot::IsSnapshotFolderName(entry->d_name))
			{
				dbg_printf("Snapshot::PurgeUserData(): INSTALLED dir = %s\n", 
					   path);
					   
				Snapshot::PurgeUserData(path, false);
				do_unlink = false;
			}
			else
			{
				dbg_printf("Snapshot::PurgeUserData(): Walking dir = %s\n", 
					   path);
					   
				Snapshot::PurgeUserData(path, do_unlink);
			}
			
			if(do_unlink == false)
			{
				char full_path[MAX_PATH_LEN];
				
				strcpy(full_path, ProxyFile::base_path);
				strcat(full_path, ProxyFile::proxy_dir);
				strcat(full_path, path);
				
				dbg_printf("Snapshot::PurgeUserData(): Remove no unlink = %s\n", 
					   	   full_path);
				
				dbg_assert(rmdir(full_path) == 0);
			}
			else
			{
				dbg_printf("Snapshot::PurgeUserData(): Remove unlink = %s\n", 
					   	   path);
				dbg_assert(cloudfs_rmdir(path) == 0);
			}
		}
		else
		{
			if(do_unlink == true)
			{
				strcpy(path, "/");
				strcat(path, start);
				strcat(path, entry->d_name);
				
				dbg_printf("Snapshot::PurgeUserData(): Unlink file = %s\n", 
					   	   path);
				
				dbg_assert(cloudfs_unlink(path) == 0);
			}
			else
			{	
				dbg_printf("Snapshot::PurgeUserData(): Remove file = %s\n", 
					   	   path);
				dbg_assert(unlink(path) == 0);
			}
		}	
	} // while
	
	dbg_assert(closedir(dir) == 0);
	
	return;
}


/*
 * DeleteSnapshot() - Remove a snapshot from cloud
 *
 * The snapshot is specified by an index into SnapshotList
 * If error return false
 */
bool Snapshot::DeleteSnapshot(int index)
{
	int ret;
	
	if(index >= SnapshotList.size())
	{
		dbg_printf("Snapshot::DeleteSnapshot(): Index out of range\n");	
		dbg_assert(0);
		return false;
	}
	
	Snapshot ss = SnapshotList[index];
	
	if(ss.installed == 1)
	{
		dbg_printf("Snapshot::DeleteSnapshot():"
				   " Snapshot currently installed\n");
		return false;
	}
	
	snapshot_ts_t ts = ss.timestamp;
	char *bucket_name = Snapshot::GetSnapshotBucketName(ts);
	char download_path[MAX_PATH_LEN];
	
	strcpy(download_path, ProxyFile::base_path);
	strcat(download_path, Segment::seg_map_filename);
	
	Snapshot::GetFileFromCloud(download_path, 
							   bucket_name, 
							   Segment::seg_map_filename);
							   
	int fd = open(download_path, O_RDONLY);
	dbg_assert(fd >= 0);
	
	// Jump over segment next ID field
	dbg_assert(lseek(fd, sizeof(seg_id_t), SEEK_CUR) >= 0);
	int seg_num = 0;
	
	ret = read(fd, &seg_num, sizeof(seg_num));
	dbg_assert(ret == sizeof(seg_num));
	
	dbg_printf("Snapshot::DeleteSnapshot(): Decreasing refcount"
			   " for %d segments\n",
			   seg_num);
	
	for(int i = 0;i < seg_num;i++)
	{
		HashObject ho;
		
		ret = read(fd, &ho, sizeof(ho));
		dbg_assert(ret == sizeof(ho));
		
		// Jump over the segment id (extent ID) field
		ret = lseek(fd, sizeof(seg_id_t), SEEK_CUR);
		dbg_assert(ret >= 0);
		
		seg_map_it it = SegmentMap.find(ho);
		
		// If we restore from a fresh disk this could happen
		if(it != SegmentMap.end())
		{
			Segment *seg_p = it->second;
			seg_p->Unlink();
		}
	}
	
	dbg_assert(close(fd) == 0);
	dbg_assert(unlink(download_path) == 0);
	
	cloud_delete_object(bucket_name, 
					    Snapshot::snapshot_proxy_filename);
	cloud_print_error();
	
	cloud_delete_object(bucket_name, 
					    Snapshot::snapshot_segment_filename);
	cloud_print_error();
	
	cloud_delete_object(bucket_name,
					    Segment::seg_map_filename);				    
	cloud_print_error();
	
	cloud_delete_object(bucket_name,
					    Segment::ext_map_filename);				    
	cloud_print_error();
	
	cloud_delete_object(bucket_name,
					    Snapshot::snapshot_disk_filename);				    
	cloud_print_error();
	
	cloud_delete_bucket(bucket_name);
	cloud_print_error();
	
	auto it = SnapshotList.begin();
	std::advance(it, index);
	// Erase the snapshot entry
	SnapshotList.erase(it);
	
	return true;
}

/*
 * Snapshot::RestoreSnapshot() - Restore a previously made snapshot
 *
 * This will delete all future snapshots automatically
 */
bool Snapshot::RestoreSnapshot(int index)
{
	int counter = 0;
	
	if(index >= SnapshotList.size())
	{
		dbg_printf("Snapshot::RestoreSnapshot(): Index out of range\n");	
		dbg_assert(0);
		return false;
	}
	
	int i = index + 1;
	while(i < SnapshotList.size())
	{
		if(SnapshotList[i].installed == 1)
		{
			return false;
		}
		
		i++;	
	}
	
	// Delete all snapshots first
	while(index < SnapshotList.size() - 1)
	{
		dbg_printf("Snapshot::RestoreSnapshot(): Delete snapshot %d\n", 
			       SnapshotList.size() - 1);
		Snapshot::DeleteSnapshot(SnapshotList.size() - 1);	
	}
	
	dbg_printf("Snapshot::RestoreSnapshot(): First delete everything we have");
	Snapshot::PurgeUserData("", true);
	
	Snapshot ss = SnapshotList[index];	
	
	snapshot_ts_t ts = ss.timestamp;
	char *bucket_name = Snapshot::GetSnapshotBucketName(ts);
	char download_path[MAX_PATH_LEN];
	
	strcpy(download_path, ProxyFile::base_path);
	strcat(download_path, Segment::ext_map_filename);
	
	dbg_printf("Snapshot::RestoreSnapshot(): Download %s\n",
		       download_path);
	Snapshot::GetFileFromCloud(download_path, 
							   bucket_name, 
							   Segment::ext_map_filename);
	
	ExtentMap.clear();
	Segment::LoadExtentMap();
	
	strcpy(download_path, ProxyFile::base_path);
	strcat(download_path, Segment::seg_map_filename);
	
	dbg_printf("Snapshot::RestoreSnapshot(): Download %s\n",
		       download_path);
	Snapshot::GetFileFromCloud(download_path, 
							   bucket_name, 
							   Segment::seg_map_filename);
							   
	SegmentMap.clear();
	Segment::LoadSegmentMap();
	
	strcpy(download_path, ProxyFile::base_path);
	strcat(download_path, Snapshot::snapshot_disk_filename);
	
	dbg_printf("Snapshot::RestoreSnapshot(): Download %s\n",
		       download_path);
	Snapshot::GetFileFromCloud(download_path, 
							   bucket_name, 
							   Snapshot::snapshot_disk_filename);
							   
	SnapshotList.clear();
	Snapshot::LoadSnapshotList();
	// This must be done because we make sure none of them
	// are installed by deleting proxy directory
	for(int i = 0;i < SnapshotList.size();i++)
	{
		SnapshotList[i].installed = 0;	
	}
	
	// Next restore proxy
	strcpy(download_path, ProxyFile::base_path);
	strcat(download_path, Snapshot::snapshot_proxy_filename);
	
	dbg_printf("Snapshot::RestoreSnapshot(): Download %s\n",
		       download_path);
	Snapshot::GetFileFromCloud(download_path, 
							   bucket_name, 
							   Snapshot::snapshot_proxy_filename);
	
	TAR *tar_p;
	tar_open(&tar_p,
			  download_path,
			  NULL,
			  O_RDONLY,
			  0644,
			  TAR_GNU | TAR_VERBOSE);
	dbg_assert(tar_p != NULL);
	
	char extract_path[MAX_PATH_LEN];
	
	strcpy(extract_path, ProxyFile::base_path);
	strcat(extract_path, ProxyFile::proxy_dir);

	dbg_printf("Snapshot::RestoreSnapshot(): Extract from %s to %s\n", 
			   download_path,
			   extract_path);
	dbg_assert(tar_extract_all(tar_p, extract_path) == 0);
	dbg_assert(tar_close(tar_p) == 0);
	
	// Remove the seg proxy file. We keep others
	dbg_assert(unlink(download_path) == 0);
	//////////////////////////////////////////////////////////////
	// Do the same
	
	strcpy(download_path, ProxyFile::base_path);
	strcat(download_path, Segment::seg_local_dir);
	
	DIR *dir = opendir(download_path);
	if(dir == NULL)
	{
		//goto restore_finish;	
	}
	
	struct dirent *entry;
	while((entry = readdir(dir)) != NULL)
	{
		counter++;
	}
	
	dbg_assert(closedir(dir) == 0);
	
	if(counter == 2)
	{
		//goto restore_finish;	
	}
	
	strcpy(download_path, ProxyFile::base_path);
	strcat(download_path, Snapshot::snapshot_segment_filename);
	
	dbg_printf("Snapshot::RestoreSnapshot(): Download %s\n",
		       download_path);
	Snapshot::GetFileFromCloud(download_path, 
							   bucket_name, 
							   Snapshot::snapshot_segment_filename);
	
	tar_open(&tar_p,
			  download_path,
			  NULL,
			  O_RDONLY,
			  0644,
			  TAR_GNU | TAR_VERBOSE);
	dbg_assert(tar_p != NULL);
	
	strcpy(extract_path, ProxyFile::base_path);
	dbg_printf("Snapshot::RestoreSnapshot(): Extract from %s to %s\n", 
			   download_path,
			   extract_path);
	dbg_assert(tar_extract_all(tar_p, extract_path) == 0);
	dbg_assert(tar_close(tar_p) == 0);
	
	dbg_assert(unlink(download_path) == 0);

restore_finish:
	////////////////////////////////////////////////////
	
	/*
	cloud_delete_object(bucket_name, 
					    Snapshot::snapshot_proxy_filename);
	cloud_print_error();
	
	cloud_delete_object(bucket_name, 
					    Snapshot::snapshot_segment_filename);
	cloud_print_error();
	
	cloud_delete_object(bucket_name,
					    Segment::seg_map_filename);				    
	cloud_print_error();
	
	cloud_delete_object(bucket_name,
					    Segment::ext_map_filename);				    
	cloud_print_error();
	
	cloud_delete_object(bucket_name,
					    Snapshot::snapshot_disk_filename);				    
	cloud_print_error();
	
	cloud_delete_bucket(bucket_name);
	cloud_print_error();
	*/
	
	return true;
}

/*
 * InstallSnapshot() - Install a snapshot to FUSE root snapshot_xxxxxxx
 *
 * If snapshot already installed then reject with false returned
 * If snapshot does not exist return false
 */
bool Snapshot::InstallSnapshot(int index)
{
	if(index >= SnapshotList.size())
	{
		dbg_printf("Snapshot::InstallSnapshot(): Index out of range\n");	
		dbg_assert(0);
		return false;
	}
	
	if(SnapshotList[index].installed == 1)
	{
		dbg_printf("Snapshot::InstallSnapshot(): Cannot install twice\n");
		return false;	
	}
	
	snapshot_ts_t ts = SnapshotList[index].timestamp;
	SnapshotList[index].installed = 1;
	
	char download_path[MAX_PATH_LEN];
	char *bucket_name = Snapshot::GetSnapshotBucketName(ts);
	
	strcpy(download_path, ProxyFile::base_path);
	strcat(download_path, Snapshot::snapshot_proxy_filename);
	
	dbg_printf("Snapshot::InstallSnapshot(): Download %s\n",
		       download_path);
	Snapshot::GetFileFromCloud(download_path, 
							   bucket_name, 
							   Snapshot::snapshot_proxy_filename);
	
	TAR *tar_p;
	tar_open(&tar_p,
			  download_path,
			  NULL,
			  O_RDONLY,
			  0644,
			  TAR_GNU | TAR_VERBOSE);
	dbg_assert(tar_p != NULL);
	
	char extract_path[MAX_PATH_LEN];
	char snapshot_dir[MAX_PATH_LEN];
	
	strcpy(extract_path, ProxyFile::base_path);
	strcat(extract_path, ProxyFile::proxy_dir);
	
	sprintf(snapshot_dir, "snapshot_%lu/", 
			(unsigned long)(ts & (0x00000000FFFFFFFFLL)));
	
	strcat(extract_path, snapshot_dir);
	
	// Create directory with fill access permission
	// This is not optimal, anyway..
	dbg_assert(mkdir(extract_path, 0777) == 0);
	
	dbg_printf("Snapshot::RestoreSnapshot(): Extract from %s to %s\n", 
			   download_path,
			   extract_path);
	dbg_assert(tar_extract_all(tar_p, extract_path) == 0);
	dbg_assert(tar_close(tar_p) == 0);
	
	// Remove the seg proxy file. We keep others
	dbg_assert(unlink(download_path) == 0);
	
	return true;
}

/*
 * UninstallSnapshot() - Uninstall an already installed snapshot
 *
 * If snapshot is not installed or index invalid return false
 * Just remove the folder created when installing the snapshot
 */
bool Snapshot::UninstallSnapshot(int index)
{
	if(index >= SnapshotList.size())
	{
		dbg_printf("Snapshot::UninstallSnapshot(): Index out of range\n");	
		dbg_assert(0);
		return false;
	}
	
	if(SnapshotList[index].installed == 0)
	{
		dbg_printf("Snapshot::UninstallSnapshot(): You must install first\n");
		return false;	
	}
	
	snapshot_ts_t ts = SnapshotList[index].timestamp;
	SnapshotList[index].installed = 0;
	
	char snapshot_dir[MAX_PATH_LEN];
	
	sprintf(snapshot_dir, "snapshot_%lu/", 
			(unsigned long)(ts & (0x00000000FFFFFFFFLL)));
	
	dbg_printf("Snapshot::UninstallSnapshot(): rmdir %s\n", snapshot_dir)
	
	// We should use path relative to /ssd/proxy
	Snapshot::PurgeUserData(snapshot_dir, false);
	
	char full_path[MAX_PATH_LEN];
	strcpy(full_path, ProxyFile::base_path);
	strcat(full_path, ProxyFile::proxy_dir);
	strcat(full_path, snapshot_dir);
	
	dbg_assert(rmdir(full_path) == 0);
	
	return true;
}
