
#include "cloudfs.h"


#define UNUSED __attribute__((unused))

#define ATTR_BUF_SIZE 256
/* Used for debugging output to /tmp/cloudfs.log */
#define debug_log(args...) do { \
		if(debug_mode) fprintf(state_.log_file, args); \
		if(print_mode) fprintf(stdout, args); \
	}while(0);

static int debug_mode = 1;
static int print_mode = 1;

static struct cloudfs_state state_;

///////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////

int cloudfs_error(const char *info)
{
	int num = errno;
	debug_log("cloudfs_error(): %s: %s\n", info, strerror(num));
	
	return -num;	
}

/*
 * Initializes the FUSE file system (cloudfs) by checking if the mount points
 * are valid, and if all is well, it mounts the file system ready for usage.
 *
 */
void *cloudfs_init(struct fuse_conn_info *conn UNUSED)
{
	debug_log("cloudfs_init(): Enter\n");
	
  	cloud_init(state_.hostname);
  	cloud_print_error();
  	
	InitProxyFile(&state_);
	InitSegment(&state_);
	InitExtent(&state_);
	InitOnlineSegmentation(&state_);
	InitOpenedFile(&state_);
	InitSnapshot(&state_);
  	
  	return NULL;
}

void cloudfs_destroy(void *data UNUSED)
{
	debug_log("cloudfs_destroy(): Enter\n");
	
  	cloud_destroy();
  	cloud_print_error();
  	
  	CleanOpenedFile();	
  	CleanOnlineSegmentation();
  	CleanExtent();
  	CleanSegment();
  	CleanProxyFile();
  	CleanSnapshot(); 
  	
  	fclose(state_.log_file);
}

/*
 * cloudfs_getattr() - Get attributes for files and non-existing files
 *
 * NOTE: DO NOT USE ProxyFile.IsDirectory() because it cannot deal with
 * non-existing files
 */
int cloudfs_getattr(const char *path, struct stat *statbuf)
{ 	
	debug_log("cloudfs_getattr(): path = %s\n", path);
	
	bool delete_flag;
	ProxyFile *pf;
	
	if(Snapshot::IsSnapshotAnchor(path))
	{
		debug_log("cloudfs_getattr(): Get attr of snapshot!\n");
		
		struct stat stat_obj;	
		pf = new ProxyFile(path);	
		
		dbg_assert(lstat(pf->full_path, &stat_obj) == 0);
		
		*statbuf = stat_obj;
		
		delete pf;
		
		return 0;
	}
	
	if(OpenedFile::FileOpened(path))
	{
		pf = OpenedFile::GetOpenedFile(path)->proxy_file;
		delete_flag = false;
	}
	else
	{
		pf = new ProxyFile(path);	
		delete_flag = true;
	}
	
	// Read first and check whether it is dir or file
	int ret = lstat(pf->full_path, statbuf);
	if(ret != 0)
	{
		if(delete_flag) 
			delete pf;
			
		return cloudfs_error("cloudfs_getattr(): ");
	}
	
	// If we are reading dir then just return; otherwise,
	// replace with proxy file
	if(!S_ISDIR(statbuf->st_mode))
	{
		if(delete_flag) 
			pf->ReadStat();
			
		*statbuf = pf->stat_obj;
	}
	
	if(delete_flag)
		delete pf;

  	return 0;
}

/*
 * cloudfs_opendir() - Open directory
 */
int cloudfs_opendir(const char *path, struct fuse_file_info *fi)
{
	DIR *dir_p;
	
	debug_log("cloudfs_opendir(): path = %s\n", path);
	ProxyFile pf(path);
	
	dir_p = opendir(pf.full_path);
	if(dir_p == NULL)
		return cloudfs_error("cloudfs_opendir(): ");
	
	/* Cookie - cast pointer to integer and next time when
	   other function is called with this fh it will be casted
	   back as a pointer */
	fi->fh = (unsigned int)dir_p;
	
	return 0;
}

/*
 * cloudfs_readdir() - Read directory
 *
 * NOTE: Compared with previous version we no longer
 * filter out lost+found folder because we are not operating 
 * under root directory
 */
int cloudfs_readdir(const char *path, void *buf, fuse_fill_dir_t filler,
			   off_t offset UNUSED, struct fuse_file_info *fi)
{
	DIR *dir_p;
	struct dirent *entry;
	int filler_ret;

	debug_log("cloudfs_readdir(): path = %s\n",
			  path);
			  
	dir_p = (DIR *)fi->fh;
	while((entry = readdir(dir_p)) != NULL)
	{		
		filler_ret = filler(buf, entry->d_name, NULL, 0);
		if(filler_ret != 0)
			return cloudfs_error("cloudfs_readdir(): ");
	}

	return 0;
}

/*
 * cloudfs_releasedir() - Release an opened directory entry
 *
 * This function is not called but we still have to implement
 */
int cloudfs_releasedir(const char *path, struct fuse_file_info *fi)
{
	int closedir_ret;

	debug_log("cloudfs_releasedir(): path = %s\n", path);

	closedir_ret = closedir((DIR *)fi->fh);
	if(closedir_ret)
		return cloudfs_error("cloudfs_releasedir(): ");
		
	return 0;
}

/*
 * cloudfs_mkdir() - Make directory
 *
 * Call mkdir() directly and let ext4 take care of everything
 */
int cloudfs_mkdir(const char *path, mode_t mode)
{
	int mkdir_ret;

	debug_log("cloudfs_mkdir(): path = %s\n", path);
	ProxyFile pf(path);
	
	mkdir_ret = mkdir(pf.full_path, mode);
	if(mkdir_ret)
		return cloudfs_error("cloudfs_mkdir(): ");

	return 0;
}


/*
 * cloudfs_mknod() - Make a file system node (i.e. regular file)
 *
 * Only supports regular file.
 * And also it relies on the face that FUSE will not
 * let us create files that already exists
 */
int cloudfs_mknod(const char *path, 
				  mode_t mode, 
				  dev_t dev UNUSED)
{
	debug_log("cloudfs_mknod(): path = %s\n", path);
	ProxyFile pf(path);
	
	if(Snapshot::IsSnapshotAnchor(path))
	{
		debug_log("cloudfs_mknod(): Trying to make anchor file\n");
		
		return -EPERM;	
	}
	
	// If file already exists or file open error then
	// assertion fails
	pf.Create(mode);
	
	return 0;
}

/*
 * cloudfs_chmod() - Change permission bits
 *
 * Calls chmod() system call
 */
int cloudfs_chmod(const char *path, mode_t mode)
{
	int chmod_ret;

	debug_log("cloudfs_chmod(): path = %s\n", path);
	
	bool delete_flag;
	ProxyFile *pf;
	
	if(Snapshot::IsSnapshotAnchor(path))
	{
		return -EPERM;
	}
	
	if(strstr(path, "/snapshot_"))
	{
		return -EPERM;	
	}
	
	if(OpenedFile::FileOpened(path))
	{
		delete_flag = false;
		pf = OpenedFile::GetOpenedFile(path)->proxy_file;
	}
	else
	{
		delete_flag = true;
		pf = new ProxyFile(path);
	}
	
	if(pf->IsDirectory())
	{
		chmod_ret = chmod(pf->full_path, mode);	
	}
	else
	{
		if(delete_flag)
			pf->ReadStat();	
			
		pf->stat_obj.st_mode = mode;
		
		if(delete_flag)
			pf->SerializeStat();
		
		chmod_ret = 0;
	}
	
	if(delete_flag)
		delete pf;

	if(chmod_ret)
		return cloudfs_error("cloudfs_chmod(): ");

	return 0;
}

/*
 * cloudfs_access - Check access for a given path
 *
 * Calls system function access() with the same signature
 */
int cloudfs_access(const char *path, int mask)
{
	int access_ret;
	
	debug_log("cloudfs_access(): path = %s\n", path);
	
	if(Snapshot::IsSnapshotAnchor(path))
	{
		debug_log("cloudfs_access(): Accessing snapshot anchor file.\n");
	}
	
	bool delete_flag;
	ProxyFile *pf;
	
	if(OpenedFile::FileOpened(path))
	{
		delete_flag = false;
		pf = OpenedFile::GetOpenedFile(path)->proxy_file;
	}
	else
	{
		delete_flag = true;
		pf = new ProxyFile(path);
	}
	
	// Not a proxy file
	if(pf->IsDirectory())
	{
		access_ret = access(pf->full_path, mask);
	}
	else
	{		
		if(delete_flag)
			pf->ReadStat();
		
		chmod(pf->full_path, pf->stat_obj.st_mode);
		access_ret = access(pf->full_path, mask);
		chmod(pf->full_path, ProxyFile::default_mode);	
	}
	
	if(delete_flag)
		delete pf;

	/* If access not granted this will return a non-zero
	   value */
	if(access_ret)
		return cloudfs_error("cloudfs_access(): ");

	return 0;
}

/*
 * cloudfs_open() - Open a regular file, or snapshot anchor
 *
 * For files other than snapshot anchor, permission bits are
 * set to allow every operation except IOCTL. For snapshot file, 
 * the only allowed operation is IOCTL.
 */
int cloudfs_open(const char *path, struct fuse_file_info *fi)
{
	OpenedFile *of;
	
	debug_log("cloudfs_open(): %s\n", path);
	
	if(OpenedFile::FileOpened(path))
	{
		debug_log("cloudfs_open(): File already opened\n");
		of = OpenedFile::GetOpenedFile(path);
		of->ref_count++;
	}
	else
	{
		IOPermission perm = IOPermission{true, true, true, true, true,
										 false, // IOCTL 
										 };
		if(Snapshot::IsSnapshotAnchor(path))
		{
			debug_log("cloudfs_open(): Opening snapshot\n");
			// It become an abstract file that supports IOCTL
			perm.ioctl = true;
			perm.read = false;
			perm.write = false;
			perm.unlink = false;
			perm.truncate = false;
			perm.chmod = false;
		}
		
		// Constructor will add itself into the table
		// Always set a permission
		of = new OpenedFile(path, perm);
	}
		
	fi->fh = (int)of;

	return 0;
}

/*
 * cloudfs_read() - Read file given an offset and size
 *
 * Call pread() instead of read() to make use of the offset
 * field. Otherwise we may have to adjust file pointer
 * before issuing any read
 */
int cloudfs_read(const char *path,
				 char *buf,
				 size_t size,
				 off_t offset,
				 struct fuse_file_info *fi)
{
	// For FUSE file, we could open it with read only mode,
	// but we could not actually read 
	if(Snapshot::IsSnapshotAnchor(path))
	{
		return -EPERM;
	}
	
	if(cloudfs_access(path, R_OK) != 0)
	{
		debug_log("cloudfs_write(): Failed in-line permission checking\n");
		return -EPERM;	
	}
	
	OpenedFile *of = (OpenedFile *)fi->fh;
	
	debug_log("cloudfs_read(): %s\n", of->proxy_file->full_path);
		
	return of->Read(buf, offset, size);
}

/*
 * cloudfs_write() - Write file given an offset and size
 *
 * Call pwrite() instead of write() to make use of the offset
 * field. Otherwise we may have to adjust file pointer
 * before issuing any read
 */
int cloudfs_write(const char *path,
				  const char *buf,
				  size_t size,
				  off_t offset,
	     		  struct fuse_file_info *fi)
{
	// We use path stored by FUSE to enforce .snapshot attributes
	if(Snapshot::IsSnapshotAnchor(path))
	{
		return -EPERM;	
	}
	
	if(strstr(path, "/snapshot_"))
	{
		return -EPERM;	
	}
	
	if(cloudfs_access(path, W_OK) != 0)
	{
		debug_log("cloudfs_write(): Failed in-line permission checking\n");
		return -EPERM;	
	}
	
	OpenedFile *of = (OpenedFile *)fi->fh;
	
	//debug_log("cloudfs_write(): fd = %s\n", of->proxy_file->full_path);
	
	return of->Write(buf, offset, size);
}


/*
 * cloudfs_release(): Close file
 *
 * Reference count is maintained by OS. If file size is greater than
 * threshold then we start migrating data, then save its stat
 * to xattr, next truncate file size to 0, and finally set on_cloud
 * to true
 */
int cloudfs_release(const char *path, struct fuse_file_info *fi)
{
	OpenedFile *of = (OpenedFile *)fi->fh;
	
	debug_log("cloudfs_release(): fd = %s\n", of->proxy_file->full_path);
	
	if(Snapshot::IsSnapshotAnchor(path))
	{
		debug_log("cloudfs_release(): Closing snapshot anchor file\n");	
	}
	
	if(of->ref_count == 1)
	{
		of->Close();
	
		// Remove that from the open file table
		delete of;
	}
	else
	{
		of->ref_count--;
	}

	return 0;
}

/*
 * cloudfs_truncate() - Truncate a file to new size
 *
 * Calls truncate() system call. This will not change
 * the offset of read write pointer
 *
 * NOTE: DO NOT USE OpenedFile::GetProxyFile()
 * we are reading the file ourselves
 */
int cloudfs_truncate(const char *path, off_t newsize)
{
	debug_log("cloudfs_truncate(): path = %s, newsize = %u\n",
			  path, 
			  (unsigned int)newsize);
			  
	if(Snapshot::IsSnapshotAnchor(path))
	{
		return -EPERM;	
	}
	
	if(strstr(path, "/snapshot_"))
	{
		return -EPERM;	
	}
	
	if(cloudfs_access(path, W_OK) != 0)
	{
		debug_log("cloudfs_write(): Failed in-line permission checking\n");
		return -EPERM;	
	}
	
	if(OpenedFile::FileOpened(path))
	{
		OpenedFile *of = OpenedFile::GetOpenedFile(path);
		of->Truncate(newsize);
		debug_log("cloudfs_truncate(): Truncate opened file\n");
	}
	else
	{
		ProxyFile *pf = new ProxyFile(path);
		pf->ReadAll();
		pf->Truncate(newsize);
		pf->SerializeAll();
		delete pf;
		
		debug_log("cloudfs_truncate(): Truncate closed file\n");
	}
		
	return 0;
}

/*
 * cloudfs_unlink() - Remove a file (hardlink)
 *
 * This is only used to remove a file, not directory
 */
int cloudfs_unlink(const char *path)
{
	debug_log("cloudfs_unlink(): path = %s\n",
			  path);
			  
	if(Snapshot::IsSnapshotAnchor(path))
	{
		return -EPERM;	
	}
	
	if(strstr(path, "/snapshot_"))
	{
		return -EPERM;	
	}

	if(OpenedFile::FileOpened(path))
	{
		debug_log("cloudfs_unlink: WARNING: DELETING AN OPENED FILE\n");
		OpenedFile *of = OpenedFile::GetOpenedFile(path);
		of->Delete();
	}
	else
	{
		ProxyFile *pf = new ProxyFile(path);
		pf->ReadAll();
		pf->Delete();

		delete pf;
	}

	return 0;
}

/*
 * cloudfs_rmdir() - Remove an empty directory
 *
 * The directory must be empty, otherwise an error
 * is returned. This feature is good for us to maintain
 * consistency between local storage and cloud
 */
int cloudfs_rmdir(const char *path)
{
	debug_log("cloudfs_rmdir(): path = %s\n", path);
	
	ProxyFile pf(path);

	if(strstr(path, "/snapshot_"))
	{
		return -EPERM;	
	}
	
	// We already know pf will be a directory because FUSE calls
	// getattr before that
	int rmdir_ret = rmdir(pf.full_path);
	if(rmdir_ret)
		return cloudfs_error("cloudfs_rmdir(): ");

	return 0;
}

/*
 * cloudfs_utime() - Change timestamp
 *
 * Calls utime() system call
 */
int cloudfs_utime(const char *path, struct utimbuf *ubuf)
{
	ProxyFile *pf;
	struct stat stat_obj;
	bool delete_flag;
	int utime_ret;
	
	debug_log("cloudfs_utime(): path = %s\n", path);
	
	if(Snapshot::IsSnapshotAnchor(path))
	{
		return -EPERM;	
	}
	
	if(strstr(path, "/snapshot_"))
	{
		return -EPERM;	
	}
	
	if(OpenedFile::FileOpened(path))
	{
		delete_flag = false;
		pf = OpenedFile::GetOpenedFile(path)->proxy_file;	
	}
	else
	{
		delete_flag = true;
		pf = new ProxyFile(path);
	}
	
	if(pf->IsDirectory())
	{
		utime_ret = utime(pf->full_path, ubuf);	
	}
	else
	{
		if(delete_flag)
			pf->ReadStat();	
			
		dbg_assert(utime(pf->full_path, ubuf) == 0);
		dbg_assert(lstat(pf->full_path, &stat_obj));
	
		pf->stat_obj.st_atime = stat_obj.st_atime;
		pf->stat_obj.st_mtime = stat_obj.st_mtime;
		
		if(delete_flag)
			pf->SerializeStat();
		
		utime_ret = 0;
	}

	if(delete_flag)
		delete pf;	
	
	if(utime_ret)
		return cloudfs_error("cloudfs_utime(): ");
	
	return 0;
}

/*
 * cloudfs_utimens() - Set time stamp with nanoseconds precision
 */
int cloudfs_utimens(const char *path, const struct timespec tv[2])
{
	ProxyFile *pf;
	struct stat stat_obj;
	bool delete_flag;
	int utimes_ret;
	
	debug_log("cloudfs_utimens(): path = %s\n", path);
	
	if(Snapshot::IsSnapshotAnchor(path))
	{
		return -EPERM;	
	}
	
	if(strstr(path, "/snapshot_"))
	{
		return -EPERM;	
	}
	
	if(OpenedFile::FileOpened(path))
	{
		delete_flag = false;
		pf = OpenedFile::GetOpenedFile(path)->proxy_file;	
	}
	else
	{
		delete_flag = true;
		pf = new ProxyFile(path);
	}
	
	if(pf->IsDirectory())
	{
		utimes_ret = utimensat(0, pf->full_path, tv, 0);	
	}
	else
	{
		if(delete_flag)
			pf->ReadStat();	
		
		dbg_assert(utimensat(0, pf->full_path, tv, 0) == 0);
		dbg_assert(lstat(pf->full_path, &stat_obj) == 0);
	
		pf->stat_obj.st_atime = stat_obj.st_atime;
		pf->stat_obj.st_mtime = stat_obj.st_mtime;
		
		if(delete_flag)
			pf->SerializeStat();
		
		utimes_ret = 0;
	}

	if(delete_flag)
		delete pf;	
	
	if(utimes_ret)
		return cloudfs_error("cloudfs_utimens(): ");
	
	return 0;
}

/*
 * cloudfs_getxattr() - Get extended attributes
 *
 */
int cloudfs_getxattr(const char *path,
				     const char *name,
					 char *value,
					 size_t size)
{
	ProxyFile pf(path);
	int getxattr_ret;

	debug_log("cloudfs_getxattr(): path = %s, key = %s, ",
			  path, name);

	getxattr_ret = lgetxattr(pf.full_path, name, value, size);
	if(getxattr_ret <= 0)
		return cloudfs_error("cloudfs_getxattr(): ");

	return getxattr_ret;
}

/*
 * cloudfs_setxattr() - Set extended attributes
 *
 */
int cloudfs_setxattr(const char *path,
					 const char *name,
					 const char *value,
					 size_t size,
					 int flags)
{
	ProxyFile pf(path);
	int setxattr_ret;
	
	if(Snapshot::IsSnapshotAnchor(path))
	{
		return -EPERM;	
	}
	
	if(strstr(path, "/snapshot_"))
	{
		return -EPERM;	
	}

	debug_log("cloudfs_setxattr(): path = %s, key = %s, value = %s",
			  path,
			  name, 
			  value);

	setxattr_ret = lsetxattr(pf.full_path, name, value, size, flags);
	if(setxattr_ret)
		return cloudfs_error("cloudfs_getxattr(): ");

	return 0;
}

/*
 * cloudfs_ioctl() - Send I/O control message to file system
 *
 * This feature is sued to implement snapshot operation.
 * Call ioctl with the file descriptor of .snapshot file
 * will send commands to the system. Ioctl() on all other
 * files are prohibited and will incur error being thrown.
 */
int cloudfs_ioctl(UNUSED const char *path, 
 				  int cmd, 
				  UNUSED void *arg, 
				  struct fuse_file_info *fi, 
				  UNUSED unsigned int flags, 
				  void *data)
{
	OpenedFile *of = (OpenedFile *)fi->fh;
	snapshot_ts_t ts, target_ts;
	unsigned long *ptr = (unsigned long *)data;
	int index;
	
	// Operation not permitted
	// This always holds for regular files
	if(of->permission.ioctl == false) 
	{
		debug_log("cloudfs_ioctl(): %s not anchor file\n", path);
		
		return -EPERM; 
	}
	
	debug_log("cloudfs_ioctl(): command = %d\n", cmd);
	
	switch(cmd)
	{
		case CLOUDFS_SNAPSHOT:
			if(SnapshotList.size() == CLOUDFS_MAX_NUM_SNAPSHOTS)
			{
				return -EPERM;
			}
			
			ts = Snapshot::TakeSnapshot();		
			*(snapshot_ts_t *)data = ts;
			return 0;
		case CLOUDFS_DELETE:
			target_ts = *(snapshot_ts_t *)data;
			
			debug_log("cloudfs_ioctl(): delete timestamp = %llu\n", 
					  target_ts);
			debug_log("cloudfs_ioctl(): Crrently %d snapshots\n", 
					  SnapshotList.size());
			
			for(int i = 0;i < SnapshotList.size();i++)
			{
				Snapshot ss = SnapshotList[i];
				debug_log("cloudfs_ioctl(): Current comparing: %llu %lu\n", 
						  ss.timestamp,
						  ss.timestamp & ((unsigned long long)0x00000000FFFFFFFFLL));
				if(target_ts == (ss.timestamp & 0x00000000FFFFFFFFLL))
				{
					bool ret = Snapshot::DeleteSnapshot(i);
					if(ret == false)
					{
						return -EBADF;
					}
					else
					{
						return 0;	
					}
				}
			}
			
			return -EBADF;
		case CLOUDFS_RESTORE:
			target_ts = *(snapshot_ts_t *)data;
			
			debug_log("cloudfs_ioctl(): RESTORE timestamp = %llu\n", 
					  target_ts);
			debug_log("cloudfs_ioctl(): Crrently %d snapshots\n", 
					  SnapshotList.size());
			
			for(int i = 0;i < SnapshotList.size();i++)
			{
				Snapshot ss = SnapshotList[i];
				debug_log("cloudfs_ioctl(): Current comparing: %llu(%lu)\n", 
						  ss.timestamp,
						  ss.timestamp & ((unsigned long long)0x00000000FFFFFFFFLL));
						  
				if(target_ts == (ss.timestamp & 0x00000000FFFFFFFFLL))
				{
					bool ret = Snapshot::RestoreSnapshot(i);
					if(ret == false)
					{
						return -EBADF;
					}
					else
					{
						return 0;	
					}
				}
			}
			
			return -EBADF;
		case CLOUDFS_INSTALL_SNAPSHOT:
			target_ts = *(snapshot_ts_t *)data;
			
			debug_log("cloudfs_ioctl(): INSTALL timestamp = %llu\n", 
					  target_ts);
			
			for(int i = 0;i < SnapshotList.size();i++)
			{
				Snapshot ss = SnapshotList[i];
				debug_log("cloudfs_ioctl(): Current comparing: %llu(%lu)\n", 
						  ss.timestamp,
						  ss.timestamp & ((unsigned long long)0x00000000FFFFFFFFLL));
						  
				if(target_ts == (ss.timestamp & 0x00000000FFFFFFFFLL))
				{
					bool ret = Snapshot::InstallSnapshot(i);
					if(ret == false)
					{
						return -EBADF;
					}
					else
					{
						return 0;	
					}
				}
			}
			
			return -EBADF;
		case CLOUDFS_UNINSTALL_SNAPSHOT:
			target_ts = *(snapshot_ts_t *)data;
			
			debug_log("cloudfs_ioctl(): UNINSTALL timestamp = %llu\n", 
					  target_ts);
			
			for(int i = 0;i < SnapshotList.size();i++)
			{
				Snapshot ss = SnapshotList[i];
				debug_log("cloudfs_ioctl(): Current comparing: %llu(%lu)\n", 
						  ss.timestamp,
						  ss.timestamp & ((unsigned long long)0x00000000FFFFFFFFLL));
						  
				if(target_ts == (ss.timestamp & 0x00000000FFFFFFFFLL))
				{
					bool ret = Snapshot::UninstallSnapshot(i);
					if(ret == false)
					{
						return -EBADF;
					}
					else
					{
						return 0;	
					}
				}
			}
			
			return -EBADF;
		case CLOUDFS_SNAPSHOT_LIST:
			for(int i = 0;i < SnapshotList.size();i++)
			{
				*ptr = (unsigned long)(SnapshotList[i].timestamp & \
									   0x00000000FFFFFFFFLL);
				ptr++;	
			}
			
			*ptr = 0UL;
			
			break;
		case 0x100:
			dbg_printf("***** INTERNAL TEST ******\n");
			dbg_printf("PURGE ALL USER DATA\n");
			Snapshot::PurgeUserData("", true);
			break;
		case 0x101:
			dbg_printf("***** INTERNAL TEST ******\n");
			dbg_printf("LIST ALL SNAPSHOTS FROM THE CLOUD\n");
			Snapshot::RebuildSnapshotListFromCloud();
			break;
		default:
			break;	
	}
	
	return 0;
}

/*
 * Functions supported by cloudfs 
 */
static struct fuse_operations cloudfs_operations;

int cloudfs_start(struct cloudfs_state *state,
                  const char* fuse_runtime_name)
{

  	int argc = 0;
  	char* argv[10];
  	FILE *fp;

	argv[argc] = (char *) malloc(128 * sizeof(char));
 	strcpy(argv[argc++], fuse_runtime_name);
 	argv[argc] = (char *) malloc(1024 * sizeof(char));
 	strcpy(argv[argc++], state->fuse_path);
  	argv[argc++] = "-s"; // set the fuse mode to single thread
	//argv[argc++] = "-f"; // run fuse in foreground

  	state_  = *state;

	fp = fopen("/tmp/cloudfs.log", "w");
	dbg_assert(fp != NULL);
	
	/* Disable buffering to make sure logs are always up to date */
	setvbuf(fp, NULL, _IOLBF, 0);
  	state_.log_file = fp;

	cloudfs_operations.init = cloudfs_init;
	cloudfs_operations.destroy = cloudfs_destroy;
	cloudfs_operations.getattr = cloudfs_getattr;
	cloudfs_operations.opendir = cloudfs_opendir;
	cloudfs_operations.readdir = cloudfs_readdir;
	cloudfs_operations.releasedir = cloudfs_releasedir;
	cloudfs_operations.mkdir = cloudfs_mkdir;
	cloudfs_operations.mknod = cloudfs_mknod;
	cloudfs_operations.access = cloudfs_access;
	cloudfs_operations.open = cloudfs_open;
	cloudfs_operations.read = cloudfs_read;
	cloudfs_operations.write = cloudfs_write;
	cloudfs_operations.release = cloudfs_release;
	cloudfs_operations.truncate = cloudfs_truncate;
	cloudfs_operations.unlink = cloudfs_unlink;
	cloudfs_operations.rmdir = cloudfs_rmdir;
	cloudfs_operations.chmod = cloudfs_chmod;
	cloudfs_operations.utime = cloudfs_utime;
	cloudfs_operations.utimens = cloudfs_utimens;
	cloudfs_operations.getxattr = cloudfs_getxattr;
	cloudfs_operations.setxattr = cloudfs_setxattr;
	cloudfs_operations.ioctl = cloudfs_ioctl;

  	int fuse_stat = fuse_main(argc, argv, &cloudfs_operations, NULL);
    
  	return fuse_stat;
}
