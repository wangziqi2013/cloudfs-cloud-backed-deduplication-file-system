
#include "cloudfs.h"

// Determine whether a segment exists or not, if a segment exists
// then change something through returned pointer
unordered_map<HashObject, Segment *, HashObjectHasher> SegmentMap;

// Given a extent, retrirve related segment information associated
// with the extent
unordered_map<ext_id_t, Segment *> ExtentMap;

const char Segment::seg_bucket_name[] = "segments";

// It must be hidden file otherwise we would fail
const char Segment::seg_map_filename[] = ".segment.map";
const char Segment::ext_map_filename[] = ".extent.map";

const char Segment::seg_local_dir[] = "segments/";
seg_id_t Segment::segment_id_next = 0;
char Segment::base_path[MAX_PATH_LEN] = "";
bool Segment::dedup_enabled = true;

////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////

void InitSegment(cloudfs_state *state)
{
	state = state;
	
	SegmentMap.clear();
	ExtentMap.clear();
	strcpy(Segment::base_path, state->ssd_path);
	
	// After mount and remount the path might already exists
	cloud_create_bucket(Segment::seg_bucket_name);
  	cloud_print_error();
  	
	// If we have a previously saved file then this will be
	// overwritten later when loading the file
	Segment::segment_id_next = Segment::segment_id_start;
	
	// This must be called before segment map loading
	Segment::LoadExtentMap();
	Segment::LoadSegmentMap();
	Segment::InitSegmentDirectory();
	
	Segment::dedup_enabled = (state->no_dedup) ? false : true;
	
	return;	
}

void CleanSegment()
{
	Segment::SaveExtentMap();
	Segment::SaveSegmentMap();
	Segment::FreeSegmentMap();	
	
	return;	
}

/*
 * InitSegmentDirectory() - Creates directory for storing local segments
 */
void Segment::InitSegmentDirectory()
{
	char seg_dir_path[MAX_PATH_LEN];
	
	strcpy(seg_dir_path, Segment::base_path);
	strcat(seg_dir_path, Segment::seg_local_dir);
	
	dbg_printf("Segment::InitSegmentDirectory(): %s\n",
			   seg_dir_path);
	
	int ret = mkdir(seg_dir_path,
					S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
	
	if(ret != 0)
	{
		if(errno == EEXIST)
		{
			dbg_printf("Segment::InitSegmentDirectory(): %s exists!\n",
					   seg_dir_path);
		}
		else
		{
			dbg_printf("Segment::InitSegmentDirectory(): mkdir error!\n");
			dbg_assert(0);
		}
	}
	
	return;
}

/*
 * LoadSegmentMap() - Load segment map into memory from disk file
 *
 * Layout
 * - sizeof(seg_id_t) Next segment ID
 * - sizeof(int) Segment count
 * - sizeof(HashObject) SegmentMap key
 * - sizeof(seg_id_t) ExtentMap key
 */
void Segment::LoadSegmentMap()
{
	char seg_map_path[MAX_PATH_LEN];
	
	strcpy(seg_map_path, Segment::base_path);
	strcat(seg_map_path, Segment::seg_map_filename);
	
	dbg_printf("Segment::LoadSegmentMap(): %s\n", seg_map_path);
	
	int fd = open(seg_map_path, 
			      O_EXCL | O_CREAT | O_RDONLY,
				  S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
	if(fd < 0)
	{
		if(errno == EEXIST)
		{
			dbg_printf("Segment::LoadSegmentMap(): Found file!\n");
		} 
		else 
		{
			int no = errno;
			dbg_printf("Segment::LoadSegmentMap(): Failed for some reason!\n");
			dbg_printf("%s\n", strerror(no));
			dbg_assert(0);
		}
	}
	else
	{
		// Close the file
		dbg_assert(close(fd) == 0);
		dbg_assert(unlink(seg_map_path) == 0);
		dbg_printf("Segment::LoadSegmentMap():"
				   " Not found. Init to empty\n");
		SegmentMap.clear();
			
		return;
	
	}
	
	// Open for read-only mode. Now we know it must exist
	fd = open(seg_map_path, O_RDONLY);
	dbg_assert(fd > 0);
	
	int map_size;
	int ret;
	seg_id_t next_seg_id;
	
	// Read in the next segment ID
	ret = read(fd, &next_seg_id, sizeof(next_seg_id));
	dbg_assert(ret == sizeof(next_seg_id));
	
	// Initialize next segment ID
	Segment::segment_id_next = next_seg_id;
	
	ret = read(fd, &map_size, sizeof(map_size));
	dbg_assert(ret == sizeof(map_size));
	
	dbg_printf("Segment::LoadSegment(): Size = %d\n", map_size);
	
	for(int i = 0;i < map_size;i++)
	{
		HashObject ho;
		ret = read(fd, &ho, sizeof(ho));
		dbg_assert(ret == sizeof(ho));
		
		seg_id_t new_seg_id;
		
		ret = read(fd, &new_seg_id, sizeof(new_seg_id));
		dbg_assert(ret == sizeof(new_seg_id));
		
		// Make sure it exists (i.e. ready to be read in)
		dbg_assert(ExtentMap.find((ext_id_t)new_seg_id) != ExtentMap.end());
		// Make sure it does not exist
		dbg_assert(SegmentMap.find(ho) == SegmentMap.end());
		
		Segment *seg_p = ExtentMap[(ext_id_t)new_seg_id];
		// Fill in MD5 part
		seg_p->md5 = ho;
		
		// Fetch the reference from extent map
		SegmentMap[ho] = seg_p;
	}
	
	dbg_assert(close(fd) == 0);
	
	return;
}

/*
 * LoadExtentMap() - Load extent map from disk on startup
 *
 * Extent Disk File Layout:
 *  - sizeof(int) Number of entries
 *  - sizeof(ext_id_t) Key in extent map
 *  - sizeof(off_t) seg_p->size
 *  - sizeof(int) seg_p->reference count
 *  - sizeof(int) seg_p->is_on_cloud
 * (We do not store md5 and id)
 * ID field is restored using the map key
 * MD5 field is restored using HashObject stored in segment map file
 */
void Segment::LoadExtentMap()
{
	char ext_map_path[MAX_PATH_LEN];
	
	strcpy(ext_map_path, Segment::base_path);
	strcat(ext_map_path, Segment::ext_map_filename);
	
	dbg_printf("Segment::LoadExtentMap(): %s\n", ext_map_path);
	
	int fd = open(ext_map_path, 
			      O_EXCL | O_CREAT | O_RDONLY,
				  S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
	if(fd < 0)
	{
		if(errno == EEXIST)
		{
			dbg_printf("Segment::LoadExtentMap(): Found file!\n");
		} 
		else 
		{
			int no = errno;
			dbg_printf("Segment::LoadExtentMap(): Failed!\n");
			dbg_printf("%s\n", strerror(no));
			dbg_assert(0);
		}
	}
	else
	{
		dbg_assert(close(fd) == 0);
		dbg_assert(unlink(ext_map_path) == 0);
		dbg_printf("Segment::LoadExtentMap():"
				   " Not found. Init to empty\n");
					   
		ExtentMap.clear();
			
		return;
	}
	
	fd = open(ext_map_path, O_RDONLY);
	dbg_assert(fd > 0);
	
	int map_size;
	int ret;
	
	ret = read(fd, &map_size, sizeof(map_size));
	dbg_assert(ret == sizeof(map_size));
	
	dbg_printf("Segment::LoadExtentMap(): Loading %d extent map entries\n",
			   map_size);
	
	for(int i = 0;i < map_size;i++)
	{
		ext_id_t new_ext_id;
		ret = read(fd, &new_ext_id, sizeof(new_ext_id));
		dbg_assert(ret == sizeof(new_ext_id));
		
		Segment *seg_p = new(Segment);
		
		ret = read(fd, &seg_p->size, sizeof(seg_p->size));
		dbg_assert(ret == sizeof(seg_p->size));
		
		ret = read(fd, &seg_p->ref_count, sizeof(seg_p->ref_count));
		dbg_assert(ret == sizeof(seg_p->ref_count));
		
		ret = read(fd, &seg_p->is_on_cloud, sizeof(seg_p->is_on_cloud));
		dbg_assert(ret == sizeof(seg_p->is_on_cloud));
		
		// We do not store seg_p->id to file
		seg_p->id = (seg_id_t)new_ext_id;
		// We do not store seg_p->md5 to file
		
		dbg_assert(ExtentMap.find(new_ext_id) == ExtentMap.end());
		ExtentMap[new_ext_id] = seg_p;
	}
	
	dbg_assert(close(fd) == 0);
	
	return;
}

/*
 * SaveSegmentMap() - Check LoadSegmentMap() for file layout
 *
 * NOTE: It does not delete segments. Call FreeSegmentMap() to free
 */
void Segment::SaveSegmentMap()
{
	char seg_map_path[MAX_PATH_LEN];
	int ret;
	
	strcpy(seg_map_path, Segment::base_path);
	strcat(seg_map_path, Segment::seg_map_filename);
	
	dbg_printf("Segment::SaveSegmentMap(): %s\n", seg_map_path);
	
	int fd = open(seg_map_path, 
			      O_CREAT | O_WRONLY | O_TRUNC,
				  S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
	dbg_assert(fd > 0);
	
	ret = write(fd, 
				&Segment::segment_id_next, 
				sizeof(Segment::segment_id_next));
	dbg_assert(ret == sizeof(Segment::segment_id_next));
	
	int map_size = SegmentMap.size();
	
	ret = write(fd, &map_size, sizeof(map_size));
	dbg_assert(ret == sizeof(map_size));
	
	seg_map_it it;
	for(it = SegmentMap.begin();it != SegmentMap.end();it++)
	{
		Segment *seg_p = it->second;
		const HashObject *ho_p = &it->first;
		
		// Write HashObject
		ret = write(fd, ho_p, sizeof(*ho_p));
		dbg_assert(ret == sizeof(*ho_p));
		
		ret = write(fd, &seg_p->id, sizeof(seg_p->id));
		dbg_assert(ret == sizeof(seg_p->id));
	}
	
	dbg_assert(close(fd) == 0);
	
	return;
}


/*
 * SaveExtentMap() - Refer to LoadExtentMap
 *
 * NOTE: It does not delete segments. Call FreeSegmentMap()
 * to avoid memory leak
 */
void Segment::SaveExtentMap()
{
	char ext_map_path[MAX_PATH_LEN];
	int ret;
	
	strcpy(ext_map_path, Segment::base_path);
	strcat(ext_map_path, Segment::ext_map_filename);
	
	dbg_printf("Segment::SaveExtentMap(): %s\n", ext_map_path);
	
	int fd = open(ext_map_path, 
			      O_CREAT | O_WRONLY | O_TRUNC,
				  S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
	
	dbg_assert(fd > 0);
	
	int map_size = ExtentMap.size();
	
	ret = write(fd, &map_size, sizeof(map_size));
	dbg_assert(ret == sizeof(map_size));
	
	ext_map_it it;
	for(it = ExtentMap.begin();it != ExtentMap.end();it++)
	{
		Segment *seg_p = it->second;
		ext_id_t id = it->first;
		
		ret = write(fd, &id, sizeof(id));
		dbg_assert(ret == sizeof(id));
		
		ret = write(fd, &seg_p->size, sizeof(seg_p->size));
		dbg_assert(ret == sizeof(seg_p->size));
		
		ret = write(fd, &seg_p->ref_count, sizeof(seg_p->ref_count));
		dbg_assert(ret == sizeof(seg_p->ref_count));
		
		ret = write(fd, &seg_p->is_on_cloud, sizeof(seg_p->is_on_cloud));
		dbg_assert(ret == sizeof(seg_p->is_on_cloud));
	}
	
	dbg_assert(close(fd) == 0);
	return;
}

/*
 * Segment::FreeSegmentMap() - Release memory allocated to in-memory
 * representation of segments.
 *
 * This is called when we are unmounting file system
 * This should not be called when we just want to take a dump
 * of the segment/extent metadata.
 */
void Segment::FreeSegmentMap()
{
	seg_map_it it;
	int seg_num = SegmentMap.size();
	
	dbg_printf("Segment::FreeSegmentMap(): Free %d segments\n", seg_num);
	
	for(it = SegmentMap.begin();it != SegmentMap.end();it++)
	{
		Segment *seg_p = it->second;
		
		delete seg_p;
	}
	
	return;
}


/*
 * GetMD5String() - Return a string representation of MD5 hash value
 *
 * Returned string are created in a static local variable. Do not
 * free that pointer. The content is valid until next call.
 */
char *Segment::GetMD5String()
{
	// MD5_DIGEST_LENGTH is a byte length
	// each byte is representable using 2 hex digits
	// and we need an extra '\0'
	static char md5_str[MD5_DIGEST_LENGTH * 2 + 1];
	char *ptr = md5_str;
	
	// It outputs 8 bytes (16 char) plus a '\0' each time
	// we advance ptr by 16 bytes to overwrite
	// non-terminating '\0' but keep the latter one
	sprintf(ptr, "%.16llX", this->md5.msb);
	ptr += 16;
	sprintf(ptr, "%.16llX", this->md5.lsb);

	dbg_printf("Segment::GetMD5String(): %s\n", md5_str);

	return md5_str;
}

/*
 * GetFileName() - Return local file name for a segment
 *
 * Name format: [base_path]/segments/[MD5].[ID]
 */
char *Segment::GetFileName()
{
	static char filename[MAX_PATH_LEN];
	
	strcpy(filename, Segment::base_path);
	strcat(filename, Segment::seg_local_dir);
	strcat(filename, this->GetName());
	
	return filename;	
}

/*
 * GetName() - Returns file name for segment on disk
 *
 * The file name should serve two goals: (1) Identify
 * segment (2) Work under either dedup and non-dedup mode
 *
 * We choose "MD5.ID" format name to support both modes.
 * Under dedup mode, we assign the same ID for segments with
 * the same MD5, so they save space; Under non-dedup mode,
 * we always assign a new ID for new segment, such that
 * even if two segments have the same MD5 they are still
 * treated as two distince segments.
 */
char *Segment::GetName()
{
	static char name[MD5_DIGEST_LENGTH * 2 + 			// MD5
					 sizeof(unsigned long long) * 2 + 	// ID
					 1 + 								// .
					 1];								// 	'\0'
	
	sprintf(name, 
			"%.16llX%.16llX.%llX", 
			this->md5.msb,
			this->md5.lsb,
			this->id);
	
	dbg_printf("Segment::GetName(): %s\n", name);
	
	return name;
}

/*
 * ComputeMD5 - Return an object holding the hash value
 */
HashObject Segment::ComputeMD5(void *buffer, off_t len)
{
	MD5_CTX ctx;
	unsigned char md5[MD5_DIGEST_LENGTH];
	// I heavily relied on this fact
	dbg_assert(MD5_DIGEST_LENGTH == 16);
	
	MD5_Init(&ctx);
	MD5_Update(&ctx, buffer, len);
	MD5_Final(md5, &ctx);
	
	HashObject ho;
	// LSB are lower 64 bits
	ho.lsb = *(unsigned long long *)md5;
	// MSB are higher 64 bits
	ho.msb = *(((unsigned long long *)md5) + 1);
	
	return ho;
}

/*
 * cloud_get_buffer - Global variable
 *
 * Represents current read position into the user provided buffer
 * when reading segments from the cloud
 *
 * cloud_get_callback - Callback function used by cloud
 *
 * NOTE: cloud interface does not allow any third-party buffer 
 * to be passed as argument, therefore we have to create a 
 * global variable. 
 */
static unsigned char *cloud_get_buffer;
static int cloud_get_callback(const char *buffer, int len)
{
	memcpy(cloud_get_buffer, buffer, len);
	cloud_get_buffer += len;
	
	return len;
}

/*
 * GetDataFromCloud() - Fill buffer with segment data stored on cloud
 *
 * Assume the given buffer is long enough to hold the data
 */
void Segment::GetDataFromCloud(void *buffer)
{
	char *name = this->GetName();
	
	dbg_printf("Segment::GetDataFromCloud(): Name = %s\n", name);
	
	// Reset get buffer to write into the buffer
	// in the argument
	cloud_get_buffer = (unsigned char *)buffer;
	cloud_get_object(Segment::seg_bucket_name,
					 name,
					 cloud_get_callback);
	cloud_print_error();
	
	return;
}

/*
 * GetDataFromFile() - Read segment stored locally
 *
 * It assumes the file already exists. If not then assertion fails
 */
void Segment::GetDataFromFile(void *buffer)
{
	char *filename = this->GetFileName();
	
	dbg_printf("Segment::GetDataFromFile(): %s\n", filename);
	
	int fd = open(filename, O_RDONLY);
	dbg_assert(fd >= 0);
	
	int ret = read(fd, buffer, this->size);
	dbg_assert(ret == this->size);
	
	dbg_assert(close(fd) == 0);
	
	return;
}

/*
 * GetData() - Read segment data which is transparent regarding its
 * location.
 *
 * It multiplex the request to two member functions according to the
 * state of current segment (on cloud or not).
 */
void Segment::GetData(void *buffer)
{
	if(this->is_on_cloud == 0)
		this->GetDataFromFile(buffer);
	else
		this->GetDataFromCloud(buffer);
	
	return;
}

/*
 * PutData() - Add a new segment
 * 
 * If dedup is enabled first it checks MD5 sum to make sure
 * the segment with that MD5 does not yet exist. If it exists
 * then just increase reference count, otherwise create a new segment
 * and upload data.
 */
Segment *Segment::PutData(void *buffer, 
						  off_t len, 
						  SegmentPlacementHint sph)
{
	HashObject ho = Segment::ComputeMD5(buffer, len);
	seg_map_it it = SegmentMap.find(ho);
	
	// If dedup is truned off, we will create segment every time
	// this is called, and we do not need to worry about naming problem
	// since segment name is a combination of MD5 and ID
	// as long as ID are unique there is no name collision
	if(Segment::dedup_enabled == false || it == SegmentMap.end())
	{
		dbg_printf("Segment::PutData(): Segment Not Found! Create New!\n");
		seg_id_t id = Segment::segment_id_next;
		Segment::segment_id_next++;
		
		Segment *seg_p = new(Segment);
		seg_p->id = id;
		seg_p->md5 = ho;
		seg_p->ref_count = 1;
		seg_p->size = len;
		
		SegmentMap[ho] = seg_p;
		ExtentMap[id] = seg_p;
		
		if(sph.placement == sph.upload_all || 
		   sph.placement == sph.upload_this)
		{
		   seg_p->UploadDataToCloud(buffer, len);
		   seg_p->is_on_cloud = 1;
		}
		else if(sph.placement == sph.store_on_local)
		{
			seg_p->UploadDataToFile(buffer, len);	
			seg_p->is_on_cloud = 0;
		}
		else
		{
			dbg_printf("Segment:PutData(): Unknown hint: %d\n", 
					   sph.placement);	
			dbg_assert(0);
		}
		
		if(sph.placement == sph.upload_all)
		{
			dbg_printf("Segment::PutData(): Upload data in journal!\n");
			Segment::UploadDataInJournalToCloud(sph.journal);	
		}
		
		return seg_p;
	} 
	else 
	{
		dbg_printf("Segment::PutData(): Segment found!\n");	
		Segment *seg_p = it->second;
		seg_p->ref_count++;
		dbg_printf("Segment::PutData(): md5 = %s, ref_count = %d\n",
				   seg_p->GetMD5String(),
				   seg_p->ref_count);
		
		return seg_p;
	}
}

/*
 * UploadDataInJournalToCloud() - Upload extents (i.e. segments) in
 * a given journal to the cloud and delete local copies.
 */
void Segment::UploadDataInJournalToCloud(vector<Extent *> *journal)
{
	int ext_num = journal->size();
	
	dbg_printf("Segment::UploadDataInJournalToCloud(): %d segments\n",
			   ext_num);
	
	for(int i = 0;i < ext_num;i++)
	{
		Extent *ext_p = (*journal)[i];
		Segment *seg_p = ext_p->GetSegment();
		
		if(seg_p->is_on_cloud == true)
			continue;
		
		void *buffer = malloc(seg_p->size);
		seg_p->GetDataFromFile(buffer);
		seg_p->UploadDataToCloud(buffer, seg_p->size);
		free(buffer);
		
		seg_p->CleanDataOnFile();
		seg_p->is_on_cloud = 1;
	}
	
	return;
}

/*
 * cloud_put_buffer - Global variable
 * cloud_put_callback() - Callback
 */
static unsigned char *cloud_put_buffer;
static int cloud_put_callback(char *buffer, int len)
{	
	memcpy(buffer, cloud_put_buffer, len);
	cloud_put_buffer += len;

	return len;
}

/*
 * UploadDataToCloud() - Upload data to cloud storage
 */
void Segment::UploadDataToCloud(void *buffer, off_t len)
{
	char *name = this->GetName();
	
	dbg_printf("Segement::UploadDataToCloud(): name = %s, len = %lld\n", 
			   name,
			   len);
	
	cloud_put_buffer = (unsigned char *)buffer;
	cloud_put_object(Segment::seg_bucket_name,
					 name,
					 len,
					 cloud_put_callback);
	cloud_print_error();
	
	return;
}

/*
 * UploadDataToFile() - Save segment data to a local file
 */
void Segment::UploadDataToFile(void *buffer, off_t len)
{	
	char *filename = this->GetFileName();
	
	dbg_printf("Segment::UploadDataToFile(): %s\n", filename);
	
	// Open file for read only, truncate if exists, create if not exists
	int fd = open(filename, 
			      O_WRONLY | O_CREAT | O_TRUNC,
				  S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
	dbg_assert(fd >= 0);
	
	int ret = write(fd, buffer, len);
	dbg_assert(ret == len);
	
	dbg_assert(close(fd) == 0);
	
	return;
}

/*
 * CleanDataOnCloud() - Cleanup the segment on the cloud
 */
void Segment::CleanDataOnCloud()
{
	char *name = this->GetName();
	
	dbg_printf("Segment::CleanDataOnCloud(): %s\n", name)
	
	cloud_delete_object(Segment::seg_bucket_name, 
						name);
	cloud_print_error();
	
	return;
}

/*
 * CleanDataOnFile() - Delete local file that stores a segment
 */
void Segment::CleanDataOnFile()
{
	char *filename = this->GetFileName();
	
	dbg_printf("Segment::CleanDataOnFile(): %s\n", filename);
	
	dbg_assert(unlink(filename) == 0);
	
	return;
}

/*
 * CleanData() - Dispatcher to clean data based on is_on_cloud
 */
void Segment::CleanData()
{
	if(this->is_on_cloud == 0)
		this->CleanDataOnFile();
	else
		this->CleanDataOnCloud();
	
	return;
	
}

/*
 * Unlink() - One less reference to the segment
 *
 * If reference count decreases to 0, the reference to the
 * segment will be deleted from SegmentMap, ExtentMap and also
 * from the cloud.
 */
void Segment::Unlink()
{
	if(this->ref_count == 1)
	{
		// Make sure it exists
		dbg_assert(SegmentMap.find(this->md5) != SegmentMap.end());
		dbg_assert(ExtentMap.find(this->id) != ExtentMap.end());
		
		// That's why we have to store MD5 inside segment object
		SegmentMap.erase(this->md5);
		ExtentMap.erase(this->id);
		
		// Delete segment on the cloud or local disk
		this->CleanData();
		
		dbg_printf("Segment::Unlink(): Refcount = 1, delete segment %lld\n", 
				   this->id);
		// Commit Suicide! Exciting!
		// But make sure it's the last operation it had ever done
		// Because dead people can't talk
		delete this;
		
		return;
	}
	
	dbg_printf("Segment::Unlink(): Ref_count = %d, decrease!\n",
			   this->ref_count);
	
	this->ref_count--;
	return;
}
