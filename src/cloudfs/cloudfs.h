#ifndef __CLOUDFS_H_
#define __CLOUDFS_H_

#include <ctype.h>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <fuse.h>
#include <getopt.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/xattr.h>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>
#include "cloudapi.h"
#include "snapshot-api.h"
#include "dedup.h"
#include <vector>
#include <unordered_map>
#include <openssl/md5.h>
#include <libtar.h>

using namespace std;

#define MAX_PATH_LEN 4096
#define MAX_HOSTNAME_LEN 1024

struct cloudfs_state {
  char ssd_path[MAX_PATH_LEN];
  char fuse_path[MAX_PATH_LEN];
  char hostname[MAX_HOSTNAME_LEN];
  int ssd_size;
  int threshold;
  
  // The below three are all in units of byte
  int avg_seg_size;
  int max_seg_size;
  int min_seg_size;
  
  int rabin_window_size;
  char no_dedup;
  
  FILE *log_file;
};

struct stat;

int cloudfs_start(struct cloudfs_state* state,
                  const char* fuse_runtime_name);  
void cloudfs_get_fullpath(const char *path, char *fullpath);
char *get_xattr(const char *full_path, const char *key);
void set_xattr(const char *full_path, const char *key, const char *value);
void save_stat_to_xattr(const char *full_path, struct stat *stat_p);
void read_stat_from_xattr(const char *full_path, struct stat *stat_p, int);
char *name_on_cloud(const char *name_on_local);
void clean_on_cloud(const char *full_path);

//////////////////////////////////////////////////////////////////
// For checkpoint 2
//////////////////////////////////////////////////////////////////

#define dbg_assert(cond) do { if(!(cond)) \
	{ \
	  int _dbg_no = errno; \
	  fprintf(stdout, "Assertion fail: %s %s %d %s\n", \
			  __func__, __FILE__, __LINE__, strerror(_dbg_no)); \
	  fprintf(ProxyFile::log_file, "Assertion fail: %s %s %d %s\n", \
			  __func__, __FILE__, __LINE__, strerror(_dbg_no)); \
	  exit(1); \
		} } while(0);

#define dbg_printf(args...) do {} while(0);

/*
#define dbg_printf(args...) do { \
	fprintf(ProxyFile::log_file, args); \
	fprintf(stdout, args); \
	} while(0);
*/

void InitProxyFile(cloudfs_state *state);
void CleanProxyFile();
void InitSegment(cloudfs_state *state);
void CleanSegment();
void InitExtent(cloudfs_state *state);
void CleanExtent();
void InitOnlineSegmentation(cloudfs_state *state);
void CleanOnlineSegmentation();
void InitOpenedFile(cloudfs_state *state);
void CleanOpenedFile();
void InitSnapshot(cloudfs_state *state);
void CleanSnapshot();

// Segment ID type
typedef unsigned long long seg_id_t;
// Extent id type
typedef seg_id_t ext_id_t;

struct ProxyFile;
struct Segment;
struct Extent;
struct OnlineSegmentation;
struct OpenedFile;
struct Snapshot;

/*
 * HashObject - Stores 16 byte MD5 hash value
 *
 * To simplify hash valuemaintenance, store char[16] as
 * two unsigned long long called MSB and LSB respectively
 */
struct HashObject
{
	unsigned long long lsb;
	unsigned long long msb;

	// Operator overloading required for using unordered map
	bool operator==(const HashObject &obj) const
	{
		return (lsb == obj.lsb) && (msb == obj.msb);
	}
};

struct FileHandler
{
	char path[MAX_PATH_LEN];
	
	FileHandler(const char *s)
	{
		strcpy(path, s);
		return;	
	}
	
	FileHandler() 
	{ 
		return; 
	}
	
	FileHandler(const FileHandler &fh)
	{
		strcpy(this->path, fh.path);
		return;	
	}
		
	bool operator==(const FileHandler &obj) const
	{
		return strcmp(this->path, obj.path) == 0;
	}
};

struct FileHandlerHasher
{
	size_t operator()(const FileHandler &obj) const
	{
		int len = strlen(obj.path);
		
		unsigned int hash_val = 0x00000000;
		
		for(int i = 0;i < len;i++)
		{
			hash_val ^= (unsigned int)obj.path[i];
			hash_val *= (unsigned int)obj.path[i];	
		}
		
		return (size_t)hash_val;
	}	
};

/*
 * HashObjectHasher - Maps HashObject into a size_t hash value
 *
 * This is defined for HashObject being able to be used in unordered_map
 */
struct HashObjectHasher
{
	// Hash function defined for using unordered map
	// We just return part of its hashed value as key
	size_t operator()(const HashObject &obj) const
	{
		return *reinterpret_cast<const size_t *>(&(obj.lsb));
	}
};

/*
 * WriteObject - A reduced version of Segment
 * 
 * Used as a fast way of communicating between modules about
 * data just written into the file. Otherwise we have to query
 * segment database for segment length after segmentation
 */
struct WriteObject
{
	seg_id_t id;
	off_t size;
};

/*
 * SegmentPlacementHint - Instructs how to store segments
 *
 * The way we store segments are relevent to the size of the file
 * if a file becomes too large (larger than threshold) then all 
 * its previous segments as well as current segments will be uploaded
 * to the cloud.
 */
struct SegmentPlacementHint
{
	// Some constants that might be used (I do not use all of them)
	static const int store_on_local = 0;
	// Upload this segment as well as all segments in the journal
	static const int upload_all = 1;
	static const int download_all = 2;
	// Upload only this segment
	static const int upload_this = 3;
	static const int download_this = 4;
	
	vector<Extent *> *journal;
	int placement;
};

extern unordered_map<HashObject, Segment *, HashObjectHasher> SegmentMap;
extern unordered_map<ext_id_t, Segment *> ExtentMap;
extern unordered_map<FileHandler, 
					 OpenedFile *, 
					 FileHandlerHasher> ActiveFileMap;
extern vector<Snapshot> SnapshotList;

// Segment map iterator definition
typedef unordered_map<HashObject, Segment *, HashObjectHasher>::iterator \
	seg_map_it;
// Extent map iterator definition
typedef unordered_map<ext_id_t, Segment *>::iterator \
	ext_map_it;
typedef unordered_map<const char *, OpenedFile *>::iterator \
	active_file_it;

// Snapshot timestamp type, 64 bit
typedef unsigned long long snapshot_ts_t;

struct Snapshot
{
	static char full_path[MAX_PATH_LEN];
	static const char snapshot_filename[];
	static const char snapshot_fuse_path[];
	static const char snapshot_disk_filename[];
	static const char snapshot_proxy_filename[];
	static const char snapshot_segment_filename[];
	
	// snapshot_xxxx xxxx xxxx xxxx \0
	static const int snapshot_bucket_name_len = 16 + 9 + 1;
	
	static snapshot_ts_t GetSnapshotTimestamp();
	static char *GetSnapshotListDiskFilename();
	static char *GetSnapshotBucketName(snapshot_ts_t ts);
	static void TarProxyAndSegment();
	static void SendFileToCloud(const char *path, 
							    const char *bucket, 
								const char *name);
	static void RebuildSnapshotListFromCloud();
								
	static void GetFileFromCloud(const char *path, 
								 const char *bucket, 
								 const char *name);
	
	static void SaveSnapshotList();
	static void LoadSnapshotList();
	
	static void IncreaseAllRefcountByOne();
	static void DecreaseSomeRefCountByOne(vector<HashObject> seg_list);
	
	static bool IsSnapshotAnchor(const char *ppath)
	{
		return strcmp(ppath, Snapshot::snapshot_fuse_path) == 0;	
	}
	
	static void PurgeUserData(char *start, bool do_unlink);
	static bool IsSnapshotFolderName(char *dname);
	
	static snapshot_ts_t TakeSnapshot();
	static bool DeleteSnapshot(int index);
	static bool RestoreSnapshot(int index);
	static bool InstallSnapshot(int index);
	static bool UninstallSnapshot(int index);
			
	snapshot_ts_t timestamp;
	int installed;
};

struct IOPermission
{
	bool read;
	bool write;
	bool truncate;
	bool unlink;
	bool chmod;
	bool ioctl;
	bool _unused2;
	bool _unused3;
};

/*
 * OpenedFile - Record file R/W status
 *
 * Each opened file has an OpenedFile instance whish handles read and
 * write streaming operation on the file. On write() it detects 
 * consecutive write (i.e. current write starts exactly after previous 
 * write), and use online segmentation to split writes into segments
 * and record extents in proxy.
 *
 * If a file has been opened, all attribute change should go through
 * the opened file structure, which will be written back to disk
 * on file close. All attribute read also need to access this structure.
 */
struct OpenedFile
{
	static int size_threshold;
	static IOPermission all_permission;
	
	// Key used in ActiveFileMap
	FileHandler fh;
	IOPermission permission;
	
	// NOT USED BUT CAN BE EXTENDED IN THE FUTURE
	int ref_count;
	
	OnlineSegmentation *online_seg;
	ProxyFile *proxy_file;
	
	bool prev_write_flag;
	off_t prev_write_start;
	off_t prev_write_len;
	
	// How many bytes is cached from previous write
	off_t prev_write_cached;
	
	static bool FileOpened(const char *path);
	static OpenedFile *GetOpenedFile(const char *path);
	static ProxyFile *GetProxyFile(const char *path);
	
	OpenedFile(const char *path,
		IOPermission pperm=OpenedFile::all_permission);
	~OpenedFile();
	
	// Seal cached extent. This could do nothing for cache being empty
	void SealCachedExtent();
	// Called on every write(), record write flag and offset, len
	// If not sequential write then seal
	void KeepSequentialWrite(off_t start, off_t end);
	// This is called when we do other operations other than Write()
	void SwitchFromWrite();
	// According to LINUX read() - read exceeding the end of 
	// file will return read count only to the end of file
	size_t Read(void *buffer, off_t start, size_t size);
	// NOTE: ALWAYS RETURN REQUESTED SIZE
	size_t Write(const void *buffer, off_t start, size_t size);
	
	void Close();
	void Truncate(off_t new_size);
	void Delete();
};

/*
 * OnlineSegmentation - Splits stream of bytes to segments
 *
 * It performs segmentation in an online manner. Data are
 * provided through multiple calls, and it computes segment
 * boundary using existing data. If any boundary is found
 * it builds Segment instance and send it to segment storage.
 *
 * Corner case is that, no matter how much we have written,
 * the last chunk of data is always cached in memory and
 * not sent to segment storage before it is sealed. Therefore,
 * read after write before close may not incur POSIX compliant 
 * behaviour. To solve this, each read() must also invoke seal
 * as a safety gate.
 */
struct OnlineSegmentation
{
	// Write cache buffer size.
	// We must guarantee there is always some segment generated
	// within the buffer once it is full to make progress
	// everytime. Otherwise there will be a deadlock
	// (i.e. waiting for more data to triffer a boundary
	// while data is waiting for a bondary to be filled in)
	// We choose it to be twice the maximum segment size, because
	// we guarantee a segment after max segment size
	static off_t buffer_size;
	static int window_size;
	static int avg_seg_size;
	static int min_seg_size;
	static int max_seg_size;
	
	unsigned char *buffer;
	rabinpoly_t *rabin_poly;
	off_t len;
	
	OnlineSegmentation();
	~OnlineSegmentation();
	
	// Return segment ID which is also extent id
	seg_id_t StoreSegment(void *buffer, 
						  off_t len, 
						  SegmentPlacementHint sph);
						  
	// Return an array of IDs representing the segments stored
	vector<WriteObject> AppendData(const void *data, 
								   off_t length,
								   SegmentPlacementHint sph);
								   
	// Return true if any data is sealed
	// wo stores the segment ID and length
	bool Seal(WriteObject *wo,
			  SegmentPlacementHint sph);
};

/*
 * Extent - Maps part of a file content to a (probably shared) segment
 *
 * An extent is defined as a consecutive part of a file's content.
 * The content of a file is defined to be the union of all its contents.
 * Each extent has a unique extent ID which maps it to a segment
 * and the real content of the file is constituted by segments.
 */
struct Extent
{
	// Extent start point
	off_t start;
	// 64 bit ID
	ext_id_t id;
	
	off_t GetSize();
	Segment *GetSegment();
	
	bool CoversOffset(off_t offset);
	bool OverlapsWith(off_t start, off_t end);
	
	// It does not deal with the case when [start, end]
	// includes [this->start, this->start + this->GetSize() - 1]
	// Return values are relative to [this->start, this->end]
	// but does not exceed [start, end]. i.e. could be directly
	// used in memory copy routine
	bool IntersectionWith(off_t start, off_t end,
				          off_t *src_start, off_t *dst_start,
						  off_t *ins_len);

	// These two are specialized for [this->start, this->end] being
	// included by [start, end]
	// Returned values are relative to [start, end]
	bool IncludedBy(off_t start, off_t end);
	bool RangeInside(off_t start, off_t end,
				     off_t *src_start, off_t *dst_start,
					 off_t *ins_len);

	// Apply this extent to a buffer of range [start, end]
	bool ApplyTo(off_t start, off_t end, void *buffer);

};

struct Segment
{
	static const seg_id_t segment_id_invalid = 0;
	static const seg_id_t segment_id_start = 1;
	
	static const char seg_bucket_name[];
	static const char seg_local_dir[];
	static char base_path[MAX_PATH_LEN];
	static const char seg_map_filename[];
	static const char ext_map_filename[];
	
	// This is shared by all segments and is persistent
	// between mount/unmounts
	// Therefore it must be saved somewhere on unmount
	static seg_id_t segment_id_next;
	static bool dedup_enabled;
	
	off_t size;
	seg_id_t id;
	HashObject md5;
	unsigned int ref_count;
	int is_on_cloud;
	
	// Prints out the MD5 representation
	char *GetMD5String();
	char *GetName();
	char *GetFileName();
	
	void GetData(void *buffer);
	void GetDataFromCloud(void *buffer);
	void GetDataFromFile(void *buffer);
	
	void UploadDataToCloud(void *buffer, off_t len);
	void UploadDataToFile(void *buffer, off_t len);
	static void UploadDataInJournalToCloud(vector<Extent *> *journal);
	
	void CleanData();
	// Clean on the cloud
	void CleanDataOnCloud();
	void CleanDataOnFile();
	void Unlink();
	
	// Because we don't know whether a segment can be dedup
	// or not, must make this a static function
	static Segment *PutData(void *buffer, 
							off_t len, 
							SegmentPlacementHint sph);
							
	static HashObject ComputeMD5(void *buffer, off_t len);
	
	static void LoadSegmentMap();
	static void LoadExtentMap();
	
	static void SaveSegmentMap();
	static void SaveExtentMap();
	static void FreeSegmentMap();
	
	static void InitSegmentDirectory();
};

struct ProxyFile
{
	// Shared by all instances
	static char base_path[MAX_PATH_LEN];
	static char proxy_dir[];
	static FILE *log_file;
	static unsigned short default_mode;
	// Safeguard - initialized to 0 in constructor and set 
	// to 1 once it is loaded
	bool loaded;
	bool is_abstract;

	struct stat stat_obj;
	
	// Segment placement policy. This needs to be persisted
	SegmentPlacementHint sph;
	
	// Log structured history
	vector<Extent *> journal;
	
	// Full proxy file path (combines ssd path and
	// FUSE file path)
	char full_path[MAX_PATH_LEN];
	
	ProxyFile(const char *path, bool abstract=false);
	~ProxyFile();
	
	bool IsDirectory();
	
	struct stat *ReadStat();
	void ReadAll();
	
	struct stat *SerializeStat();
	void SerializeAll();
	
	void Create(mode_t mode);
	void Delete();
	void Truncate(off_t new_size);
private:
	void FillStat(int fd);
	void CommitStat(int fd);
};

#endif
