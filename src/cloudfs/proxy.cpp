
#include "cloudfs.h"

FILE *ProxyFile::log_file = NULL;
char ProxyFile::base_path[MAX_PATH_LEN] = "";
char ProxyFile::proxy_dir[] = "proxy/";

// Default permission bits for proxy file
// This should not change
unsigned short ProxyFile::default_mode = 0644;

////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////

/*
 * InitProxyFile() - Initialize static variables in ProxyFile
 *
 * These variables are shared by all instances of proxy files.
 * Mostly they are path names.
 */
void InitProxyFile(cloudfs_state *state)
{
	// Base path
	strcpy(ProxyFile::base_path, state->ssd_path);
	ProxyFile::log_file = state->log_file;
	
	char proxy_root_dir[MAX_PATH_LEN];
	strcpy(proxy_root_dir, ProxyFile::base_path);
	strcat(proxy_root_dir, ProxyFile::proxy_dir);
	
	// I copied mode flags from the Web, don't ask me what are they
	int ret = mkdir(proxy_root_dir,
					S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
					
	if(ret != 0)
	{
		if(errno == EEXIST)
		{
			dbg_printf("InitProxyFile(): %s already exists!\n", 
					   proxy_root_dir);
		}
		else
		{
			dbg_printf("InitProxyFile(): mkdir fails!\n");
			dbg_assert(0);	
		}
	}
	
	return;
}

void CleanProxyFile()
{
	return;
}

/*
 * Truncate() - Change file size
 *
 * We regard truncate as a method to change file size
 * If truncate is larger than current size then nothing happens
 * except the size being updated
 *
 * If truncate to 0 then all extents are freed and underlying
 * segments are unlinked.
 *
 * If truncate to a smaller value then part of the extents are
 * garbage collected (i.e. those after the truncate point)
 */
void ProxyFile::Truncate(off_t new_size)
{
	dbg_assert(this->loaded);
	
	int ext_num = this->journal.size();
	
	dbg_assert(new_size >= 0);
	
	// Update size
	this->stat_obj.st_size = new_size;
	
	// Special case
	if(new_size == 0)
	{
		dbg_printf("ProxyFile::Truncate(): to 0, free %d extents\n",
				   this->journal.size());
		for(int i = 0;i < ext_num;i++)
		{
			Extent *ext_p = this->journal[i];
			
			// Delete the underlying segment (or decrease
			// reference count)
			ext_p->GetSegment()->Unlink();

			// NOTE: DON'T DO THAT!!!!!!!!!!!!!!!!!!!!!!!!!
			// we do not know whether the segment is still alive or not
			//delete ext_p;
		}
		
		// Clear all segments
		this->journal.clear();
		
		return;
	}
	
	// The offset of the last effective byte after truncate
	off_t last_byte = new_size - 1;
	vector<Extent *> new_journal;
	int counter = 0;
	
	for(int i = 0;i < ext_num;i++)
	{
		Extent *ext_p = this->journal[i];
		
		// If the segment is totally truncated
		if(ext_p->start > last_byte)	
		{
			// Delete the underlying segment (or decrease
			// reference count)
			ext_p->GetSegment()->Unlink();
			
			counter++;
		}
		else // Segment still alive within the file
		{
			// Put into a new journal
			new_journal.push_back(ext_p);
		}
	}
	
	// Replace journal
	this->journal = new_journal;
	dbg_printf("ProxyFile::Truncate(): Freed %d extents; %d remaining\n", 
			   counter,
			   this->journal.size());
	
	return;
}

/*
 * Delete() - Remove the underlying proxy file on disk
 *
 * And before that it needs to unlink all segments
 * (just truncate to 0)
 */
void ProxyFile::Delete()
{
	dbg_assert(this->loaded);
	// This will effectively release all segments related with
	// the file
	this->Truncate(0);
	
	dbg_assert(unlink(this->full_path) == 0);
	
	return;
}

/*
 * Create() - Create a file under proxy directory
 *
 * Uses full_path. Must be called after initializer. This is usually
 * called when we create a new file
 */
void ProxyFile::Create(mode_t mode)
{
	int fd;
	
	fd = open(this->full_path, 
		      O_EXCL | O_CREAT | O_WRONLY | O_TRUNC,
			  S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH); // chmod 0644
	if(fd < 0)
	{
		if(errno == EEXIST)
		{
			dbg_printf("ProxyFile::Create(): File %s already exists"
					   " when trying to create\n",
					   this->full_path);
		} 
		else 
		{
			dbg_printf("ProxyFile::Create(): err when creating file\n");
		}
		
		dbg_assert(0); // Make sure we fail here
	}
	
	dbg_assert(close(fd) == 0);
	
	// Copy stat object of newly created file here
	dbg_assert(stat(full_path, &this->stat_obj) == 0);
	// File mode must be specified
	this->stat_obj.st_mode = mode;
	
	this->loaded = true;
	
	// Write stat into file
	this->SerializeAll();
	
	return;
}


/*
 * ProxyFile() - Constructor
 *
 * It uses the relative path to initialize full path, which might
 * be used in multiple places, so we'd better cache it for a little
 * while
 *
 * It does not load the file automatically. The caller should be
 * responsible for opening.
 */
ProxyFile::ProxyFile(const char *path, bool abstract)
{
	strcpy(this->full_path, this->base_path);
	// There are '/' before and after "proxy"
	strcat(this->full_path, "proxy");
	strcat(this->full_path, path);
	
	// Clear vector
	this->journal.clear();
	this->loaded = false;
	this->sph.placement = SegmentPlacementHint::store_on_local;
	
	this->is_abstract = abstract;
	
	return;
}

/*
 * Destructor
 *
 * Frees up all extent descriptors
 */
ProxyFile::~ProxyFile()
{
	int log_num = this->journal.size();

	for(int i = 0;i < log_num;i++)
	{
		delete this->journal[i];
	}

	return;
}

/*
 * FillStat() - Fill a stat object given file pointer
 *
 * This routine is shared by ReadStat() and ReadAll()
 */
void ProxyFile::FillStat(int fd)
{
	int read_len;
	
	if(this->is_abstract) return;
	
	dbg_printf("ProxyFile::FillStat(): Path = %s\n",
			   this->full_path);
	
	read_len = read(fd, &this->stat_obj, sizeof(this->stat_obj));
	dbg_assert(read_len == (int)sizeof(this->stat_obj));
	
	return;
}

/*
 * ReadStat() - Read struct stat from proxy file content
 *
 * Stat does not include segment information, which is only
 * used when actual file contants are involved. For some requests
 * only struct stat is required to fulfill them
 *
 * Returns the address of this->stat_obj to save some typing
 */
struct stat *ProxyFile::ReadStat()
{
	int fd;
	
	if(this->is_abstract) return NULL;
	
	this->loaded = true;
	
	dbg_printf("ProxyFile::ReadStat(): Path = %s\n",
			   this->full_path);
	
	fd = open(this->full_path, O_RDONLY);
	dbg_assert(fd >= 0);
	
	this->FillStat(fd);
	
	dbg_assert(close(fd) == 0);
	
	return &this->stat_obj;
}

/*
 * ReadAll() - Read everything in the proxy file and store them
 *
 * If we would like to access actual data through segment pointers, we
 * need to load segment pointers either.
 */
void ProxyFile::ReadAll()
{
	int fd, read_len;
	int ext_num = 0;
	
	if(this->is_abstract) return;
	
	this->loaded = true;
	
	fd = open(this->full_path, O_RDONLY);
	dbg_assert(fd >= 0);

	this->FillStat(fd);
	
	read_len = read(fd, &this->sph, sizeof(this->sph));
	dbg_assert(read_len == sizeof(this->sph));
	
	read_len = read(fd, &ext_num, sizeof(ext_num));
	dbg_assert(read_len == (int)sizeof(ext_num));
	
	dbg_printf("ProxyFile::ReadAll(): Path = %s, ext_num = %d\n",
			   full_path,
			   ext_num);
	// Make sure we won't read twice
	dbg_assert(this->journal.size() == 0);
	
	for(int i = 0;i < ext_num;i++)
	{
		// Need to free them in destructor
		Extent *ext_p = new(Extent);
		
		read_len = read(fd, &ext_p->start, sizeof(ext_p->start));
		dbg_assert(read_len == (int)sizeof(ext_p->start));
		
		read_len = read(fd, &ext_p->id, sizeof(ext_p->id));
		dbg_assert(read_len == (int)sizeof(ext_p->id));
		
		this->journal.push_back(ext_p);
	}
	
	dbg_assert(close(fd) == 0);
	
	return;
}

/*
 * CommitStat() - Write stat into given fd
 *
 * Shared routine called by Serialize* functions
 */
void ProxyFile::CommitStat(int fd)
{
	int write_len;
	
	if(this->is_abstract) return;
	
	dbg_printf("ProxuFile::CommitStat(): Write stat object for %s\n",
	           this->full_path);
	
	write_len = write(fd, &this->stat_obj, sizeof(this->stat_obj));
	dbg_assert(write_len == (int)sizeof(this->stat_obj));
	
	return;
}

struct stat *ProxyFile::SerializeStat()
{
	int fd;
	
	if(this->is_abstract) return NULL;
	
	dbg_assert(this->loaded);
	
	dbg_printf("ProxyFile::SerializeStat(): Path = %s\n",
			   this->full_path)
	
	// We do not need to truncate
	fd = open(this->full_path, O_WRONLY);
	dbg_assert(fd >= 0);

	this->CommitStat(fd);
	
	dbg_assert(close(fd) == 0);
	
	return &this->stat_obj;
}

/*
 * Serialize() - Serialize the object into a disk file
 *
 * Disk file path is given by full_path member which is
 * designated as the proxy for the file with the same
 * relative path
 *
 * This function serializes the whole stat structure,
 * number of segments, and each segment.
 */
void ProxyFile::SerializeAll()
{
	int fd, write_len;
	int ext_num;
	
	if(this->is_abstract) return;
	
	dbg_assert(this->loaded);
	
	fd = open(this->full_path, O_TRUNC | O_WRONLY);
	dbg_assert(fd >= 0);
	
	this->CommitStat(fd);
	
	// Segment placement policy
	write_len = write(fd, &this->sph, sizeof(this->sph));
	dbg_assert(write_len == sizeof(this->sph));
	
	ext_num = this->journal.size();
	
	write_len = write(fd, &ext_num, sizeof(ext_num));
	dbg_assert(write_len == (int)sizeof(ext_num));
	
	dbg_printf("ProxyFile::SerializeAll(): Path = %s, ext_num = %d\n",
			   this->full_path, ext_num);
	
	for(int i = 0;i < ext_num;i++)
	{
		Extent *ext_p = this->journal[i];
		
		write_len = write(fd, &ext_p->start, sizeof(ext_p->start));
		dbg_assert(write_len == (int)sizeof(ext_p->start));
		
		write_len = write(fd, &ext_p->id, sizeof(ext_p->id));
		dbg_assert(write_len == (int)sizeof(ext_p->id));
	}
	
	dbg_assert(close(fd) == 0);
	
	return;
}

/*
 * IsDirectory() - Judge whether the underlying full_path represents
 * a directory
 *
 * Sometimes we want to distinguish between directories and regular
 * files. If the target is a directory then any Read*() or Serialize*()
 * cannot be called (and assertion fail if you do).
 * 
 * NOTE: If not sure whether the path exists or not do not call this
 * you may have to use lstat and check return value for yourself!
 * (like in getattr() where the existence of path is not known. But
 * after point, for most of the operations it is OK to call this)
 */
bool ProxyFile::IsDirectory()
{
	struct stat temp_stat;
	
	dbg_assert(lstat(this->full_path, &temp_stat) == 0);
	
	return S_ISDIR(temp_stat.st_mode);
}
