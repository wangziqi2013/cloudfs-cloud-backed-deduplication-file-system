
#include "cloudfs.h"

void InitExtent(cloudfs_state *state)
{
	state = state;
	return;	
}

void CleanExtent()
{
	return;	
}

/*
 * GetSegment() - Return underlying segment
 */
Segment *Extent::GetSegment()
{
	return ExtentMap[this->id];
}

/*
 * GetSize() - Return segment size (which is also extent size)
 */
off_t Extent::GetSize()
{
	Segment *seg_p = this->GetSegment();
	
	dbg_assert(seg_p->size > 0)
	
	return seg_p->size;
}

/*
 * CoversOffset() - Returns true iff the offset is inside range
 *                  covered by extent
 */
bool Extent::CoversOffset(off_t offset)
{
	// NOTE: GetSize() + start is the first byte not in the
	// extent. Actual extent is in [start, start + GetSize() - 1]
	return (offset >= this->start) && \
		   (offset <= this->GetSize() + this->start - 1);
}

/*
 * OverlapsWith() - Given a range judge whether it overlaps with
 * this extent
 */
bool Extent::OverlapsWith(off_t pstart, off_t pend)
{
	dbg_assert(pstart <= pend);
	
	return this->CoversOffset(pstart) || this->CoversOffset(pend);
}

/*
 * IntersectionWith() - Return intersection between this extent
 * and a range
 *
 * Return values are in ins_start and ins_len. These two are
 * relative offset inside the extent (i.e. ins_start is relative
 * to this->start)
 *
 * Returns false if there is no intersection. Can use this funtion
 * to detect whether there is any intersection.
 */
bool Extent::IntersectionWith(off_t pstart, off_t pend,
			 		          off_t *src_start, off_t *dst_start,
							  off_t *ins_len)
{
	// Must be a valid range
	dbg_assert(pstart <= pend);
	
	if(this->OverlapsWith(pstart, pend) == false)
		return false;
	
	bool start_in_extent = this->CoversOffset(pstart);
	bool end_in_extent = this->CoversOffset(pend);
	
	// Both endpoints are in the range
	if(start_in_extent && end_in_extent)
	{
        *src_start = pstart - this->start;
        *dst_start = 0;
        *ins_len = pend - pstart + 1;
        
        dbg_printf("Extent::IntersectionWith(): src_start = %lld,"
				   " dst_start = %lld,"
				   "ins_len = %lld, (%d & %d)\n",
				   *src_start,
				   *dst_start,
				   *ins_len,
				   start_in_extent,
				   end_in_extent);
        
        return true;
	}
	
	off_t this_end = this->GetSize() + this->start - 1;

	if(start_in_extent && !end_in_extent)
	{
		*src_start = pstart - this->start;
		*dst_start = 0;
		*ins_len = this_end - pstart + 1;
		
		dbg_printf("Extent::IntersectionWith(): src_start = %lld,"
				   " dst_start = %lld,"
				   "ins_len = %lld, (%d & %d)\n",
				   *src_start,
				   *dst_start,
				   *ins_len,
				   start_in_extent,
				   end_in_extent);
				   
		return true;
	}
	
	if(!start_in_extent && end_in_extent)
	{
		*src_start = 0;
		*dst_start = this->start - pstart;
		*ins_len = pend - this->start + 1;

		dbg_printf("Extent::IntersectionWith(): src_start = %lld,"
				   " dst_start = %lld,"
				   "ins_len = %lld, (%d & %d)\n",
				   *src_start,
				   *dst_start,
				   *ins_len,
				   start_in_extent,
				   end_in_extent);
		
		return true;
	}
	
	dbg_printf("Extent::IntersectionWith(): Something impossible happens!\n");
	dbg_assert(0);
	
	return false;
}

/*
 * IncludedBy() - Judge whether this extent is included by given
 *                range [start, end]
 *
 * This function together with Extent::OverlapsWith() consitutes
 * every case of extent sharing some range with [start, end]
 */
bool Extent::IncludedBy(off_t pstart, off_t pend)
{
	dbg_assert(pstart <= pend);
	
	off_t this_end = this->start + GetSize() - 1;
	
	return (this_end <= pend && this->start >= pstart);
}

/*
 * RangeInside() - Return the range inside a larger extent
 *                 Only applies when the extent is included
 *                 by a bigger one
 *
 * Return value are offsets inside larger range.
 * ins_len is always this->GetSize() because it is included
 * in the larger extent
 */
bool Extent::RangeInside(off_t pstart, off_t pend,
					     off_t *src_start, off_t *dst_start,
						 off_t *ins_len)
{
	dbg_assert(pstart <= pend);
	
	if(this->IncludedBy(pstart, pend) == false)
		return false;
		
	*src_start = 0;
	*dst_start = this->start - pstart;
	*ins_len = this->GetSize();
	
	dbg_printf("Extent::RangeInside(): src_start = %lld,"
			   " dst_start = %lld,"
			   " ins_len = %lld\n",
			   *src_start,
			   *dst_start,
			   *ins_len);
	
	return true;
}

/*
 * ApplyTo() - If there is any overlap between the extent and
 *             given range [start, end] then copy the overlapped
			   byte to buffer.
 * Return true if any byte is copied. Otherwise false.
 * Assume the buffer is allocated somewhere, and the length
 * of the buffer is at least end - start + 1
 */
bool Extent::ApplyTo(off_t pstart, off_t pend, void *buffer)
{
	off_t src_start, dst_start, ins_len;
	
	dbg_assert(buffer);
	
	dbg_printf("Extent::ApplyTo(): buffer [%lld, %lld],"
				   " extent [%lld, %lld]",
				   pstart,
				   pend,
				   this->start,
				   this->start + this->GetSize() - 1);
	
	if(this->OverlapsWith(pstart, pend))
	{
		dbg_printf(" Overlap\n")
		this->IntersectionWith(pstart, pend,
							   &src_start, &dst_start, &ins_len);
	} else if(this->IncludedBy(pstart, pend))
	{
		dbg_printf(" Include\n");
		this->RangeInside(pstart, pend,
					      &src_start, &dst_start, &ins_len);
	} 
	else
	{
		dbg_printf(" no intersection\n");
		return false;
	}
	
	void *data = malloc(this->GetSize());
	dbg_assert(data);
	
	this->GetSegment()->GetData(data);
	
	memcpy((unsigned char *)buffer + dst_start,
		   (unsigned char *)data + src_start,
		   ins_len);
		   
	free(data);

	return true;
}
